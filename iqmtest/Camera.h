#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

//#include <GameMath/ps3/GameMath.h>
#include <gamemath/cpp/GameMath.h>
#include <MoveComponent.h>

class Camera
{
public:
    virtual GameMath::Mat4& GetViewMat() = 0;
};

class FPSCamera : public Camera
{
public:
    FPSCamera();
    ~FPSCamera();
    
    MoveComponent& GetMoveComponent() { return mMoveComponent; }
    GameMath::Vec3& GetPosition() { return mMoveComponent.position; }
    GameMath::Mat4& GetViewMat() { return mViewMat; }
    
    void Update();
    
private:
    MoveComponent mMoveComponent;
    GameMath::Mat4 mViewMat;
};

#endif // CAMERA_H_INCLUDED

