#include <Renderer.h>
#include <VertexBuffer.h>
#include <Texture.h>
#include <IqmMesh.h>
#include <TileFloor.h>
#include <Camera.h>
#include <Input.h>

static Renderer gRender;

static VertexBuffer gPlaneBuf;
static Texture gPlaneTex;
static IqmMesh gIqm;
static Texture gIqmTex;
static TileFloor gFloor;
static FPSCamera gCamera;
static Input gInput;

void InitPlaneBuf()
{
    float vertices[8*6] = {
        // position             normal              texcoord
        -0.5f, -0.5f, 0.0f,     0.0f, 0.0f, 1.0f,   0.0f, 0.0f,
        0.5f, -0.5f, 0.0f,      0.0f, 0.0f, 1.0f,   1.0f, 0.0f,
        0.5f, 0.5f, 0.0f,       0.0f, 0.0f, 1.0f,   1.0f, 1.0f,
        
        -0.5f, -0.5f, 0.0f,     0.0f, 0.0f, 1.0f,   0.0f, 0.0f,
        0.5f, 0.5f, 0.0f,       0.0f, 0.0f, 1.0f,   1.0f, 1.0f,
        -0.5f, 0.5f, 0.0f,      0.0f, 0.0f, 1.0f,   0.0f, 1.0f
    };
    // vertices, verticesSize, indices, indicesSize, floatsPerVertex
    gPlaneBuf.Init(vertices, 8*6, nullptr, 0, 8);
}
bool InitPlaneTex()
{
    return gPlaneTex.LoadTGA(ASSETS_DIR"/grass.tga");
}

static void MoveCamera()
{
    MoveComponent& mc = gCamera.GetMoveComponent();
    const float moveSpeed = 5.0f;
    if (gInput.GetLeftAnalogY() > 200) {
        mc.MoveForward(-moveSpeed * (1.0f/60.0f));
    }
    else if (gInput.GetLeftAnalogY() < 50) {
        mc.MoveForward(moveSpeed * (1.0f/60.0f));
    }

    if (gInput.GetLeftAnalogX() > 200) {
        mc.MoveRight(moveSpeed * (1.0f/60.0f));
    }
    else if (gInput.GetLeftAnalogX() < 50) {
        mc.MoveRight(-moveSpeed * (1.0f/60.0f));
    }

    const float rotSpeed = 30.0f;
    if (gInput.GetRightAnalogY() > 200) {
        mc.AddPitch(-rotSpeed * (1.0f/60.0f));
    }
    else if (gInput.GetRightAnalogY() < 50) {
        mc.AddPitch(rotSpeed * (1.0f/60.0f));
    }
    if (gInput.GetRightAnalogX() > 200) {
        mc.AddYaw(-rotSpeed * (1.0f/60.0f));
    }
    else if (gInput.GetRightAnalogX() < 50) {
        mc.AddYaw(rotSpeed * (1.0f/60.0f));
    }
}

int main()
{
    GameMath::Mat4 modelMat =
        GameMath::Translate(2.0f, 0.0f, 1.0f) *
        GameMath::Rotate(GameMath::Deg2Rad(-90.0f), GameMath::Vec3(1.0f, 0.0f, 0.0f)) *
        GameMath::Scale(0.025f, 0.025f, 0.025f);
    
    if (!gRender.Init(1024,768,"Does Nothing")) { return 1; }
    if (!gInput.Init(0)) { return 1; }
    if (!InitPlaneTex()) { return 1; }
    if (!gIqm.Init(ASSETS_DIR"/gina.iqm")) { return 1; }
    if (!gIqmTex.LoadTGA(ASSETS_DIR"/gina.tga")) { return 1; }
    {
        IqmMesh::Anim idleAnim = { 0, 180, "idle" };
        IqmMesh::Anim walkAnim = { 181, 181+30-1, "walk" };
        gIqm.AddAnim(idleAnim);
        gIqm.AddAnim(walkAnim);
        gIqm.SetAnim("idle");
    }
    
    //printf("InitPlaneBuf\n");
    InitPlaneBuf();

    if (!gFloor.Init()) { return 1; }
    gFloor.SetTexture(&gPlaneTex);
    gFloor.SetPosition(GameMath::Vec3(0.0f,0.0f,-10.0f));
    gFloor.SetScale(5.0f);

    gCamera.GetMoveComponent().position = GameMath::Vec3(0.0f, 20.0f, 20.0f);
    gCamera.GetMoveComponent().pitch = -45.0f;
    gCamera.Update();
    
    for (;;)
    {
        gInput.Update();
        gRender.Clear();

        MoveCamera();
        
        // TODO - actual timer update
        // 30 fps anim
        //gIqm.Update((1.0f / 60.0f) * 30.0f);
        gCamera.Update();

        //printf("Render set texture\n");
        //gRender.SetTexture(gPlaneTex);
        //printf("Render draw vertex buf\n");
        //gRender.DrawVertexBuffer(modelMat, viewMat, gPlaneBuf);

        //gFloor.Draw(gCamera.GetViewMat(), gRender);

        gRender.SetTexture(gIqmTex);
        gIqm.DrawVertexBuffer(modelMat, gCamera.GetViewMat(), gRender, 0);
        
        gRender.Update();
    }
    
    return 0;
}

