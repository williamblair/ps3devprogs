#include <TileFloor.h>
#include <stdio.h>

#define TILESIZE 1.0f

float TileFloor::sVertices[6*8] = {
    // position                 normal                  texcoord
//    -TILESIZE, 0.0f, TILESIZE,  0.0f, 1.0f, 0.0f,     0.0f, 0.0f,  // bottom left
//    TILESIZE, 0.0f, TILESIZE,   0.0f, 1.0f, 0.0f,     1.0f, 0.0f,  // bottom right
//    TILESIZE, 0.0f,-TILESIZE,   0.0f, 1.0f, 0.0f,     1.0f, 1.0f,  // top right
//    
//    -TILESIZE, 0.0f, TILESIZE,  0.0f, 1.0f, 0.0f,    0.0f, 0.0f, // bottom left
//    TILESIZE, 0.0f,-TILESIZE,   0.0f, 1.0f, 0.0f,    1.0f, 1.0f, // top right
//    -TILESIZE, 0.0f, -TILESIZE, 0.0f, 1.0f, 0.0f,    0.0f, 1.0f  // top left

    -TILESIZE, 0.0f, -TILESIZE, 0.0f, 1.0f, 0.0f,    0.0f, 1.0f,  // top left
    TILESIZE, 0.0f,-TILESIZE,   0.0f, 1.0f, 0.0f,    1.0f, 1.0f,  // top right
    TILESIZE, 0.0f, TILESIZE,   0.0f, 1.0f, 0.0f,    1.0f, 0.0f,  // bottom right

    -TILESIZE, 0.0f, -TILESIZE, 0.0f, 1.0f, 0.0f,    0.0f, 1.0f,  // top left
    TILESIZE, 0.0f, TILESIZE,   0.0f, 1.0f, 0.0f,    1.0f, 0.0f,  // bottom right
    -TILESIZE, 0.0f, TILESIZE,  0.0f, 1.0f, 0.0f,    0.0f, 0.0f // bottom left
};
bool TileFloor::sInitted = false;

TileFloor::TileFloor() :
    mPosition(0.0f, 0.0f, 0.0f),
    mScale(1.0f, 1.0f, 1.0f),
    mTexture(nullptr)
{
}

TileFloor::~TileFloor()
{
}

bool TileFloor::Init()
{
    if (sInitted) {
        return true;
    }
    
    //void Init(
    //    float* vertices,
    //    size_t verticesSize,
    //    int* indices,
    //    size_t indicesSize,
    //    size_t floatsPerVertex
    //)
    
    // verts, vertssize, indices, indicessize, floats per vertex
    sInitted = true;
    return mVertBuf.Init(
        sVertices, // vertices
        6*8, // vertices size
        nullptr, // indices
        0, // indicesSize
        8 // floats per vertex
    );
}

bool TileFloor::Draw(
    GameMath::Mat4& viewMat,
    Renderer& render)
{
    if (mTexture == nullptr) {
        printf("TileFloor draw tex is null\n");
        return false;
    }
    // assumes all scale components are equal
    const float tileSize = 2.0f * mScale.x;
    //const float tileSize = TILESIZE;
    float x = mPosition.x;
    float z = mPosition.z;
    const size_t numTilesX = 8;
    const size_t numTilesZ = 8;
    GameMath::Mat4 modelMat;
    
    render.SetTexture(*mTexture);
    for (size_t i = 0; i < numTilesX; ++i)
    {
        z = mPosition.z;
        for (size_t j = 0; j <numTilesZ; ++j)
        {
            modelMat = 
                GameMath::Translate(x, mPosition.y, z) *
                GameMath::Rotate(0.0f, GameMath::Vec3(1.0f, 0.0f, 0.0f)) *
                GameMath::Scale(mScale.x, 1.0f, mScale.z);
            render.DrawVertexBuffer(modelMat, viewMat, mVertBuf);
            
            z += tileSize;
        }
        x += tileSize;
    }
    return true;
}

