#include <Shader.h>

Shader::Shader() :
    mVpo(nullptr),
    mFpo(nullptr),
    mVpUCode(nullptr),
    mFpUCode(nullptr),
    mFpBuffer(nullptr),
    mFpOffset(0)
{}

Shader::~Shader()
{
}

bool Shader::Init(rsxVertexProgram* vpo, rsxFragmentProgram* fpo)
{
    mVpo = vpo;
    mFpo = fpo;
    return InitUCode();
}

bool Shader::InitUCode()
{
    u32 fpsize = 0;
    u32 vpsize = 0;
    rsxVertexProgramGetUCode(mVpo, &mVpUCode, &vpsize);
    rsxFragmentProgramGetUCode(mFpo, &mFpUCode, &fpsize);
    mFpBuffer = (u32*)rsxMemalign(64, fpsize);
    memcpy(mFpBuffer, mFpUCode, fpsize);
    rsxAddressToOffset(mFpBuffer, &mFpOffset);
    
    return true;
}
