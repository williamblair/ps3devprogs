#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <rsxutil.h>
#include <VertexBuffer.h>
#include <Shader.h>
#include <Texture.h>

//#include <GameMath/ps3/GameMath.h>
#include <gamemath/cpp/GameMath.h>

class Renderer
{
public:
    Renderer();
    ~Renderer();
    
    bool Init(int width, int height, const char* title);
    
    void Clear();
    void Update();
    
    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        VertexBuffer& vb
    );
    
    void SetTexture(Texture& tex);
    
private:
    void* mHostAddr;
    gcmContextData* mContext;
    
    Shader* mCurShader;
    Shader mTexturedShader;
    
    GameMath::Mat4 mProjMat;
    
    void SetDrawEnv();
};

#endif // RENDERER_H_INCLUDED
