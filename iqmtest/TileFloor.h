#ifndef TILE_FLOOR_H_INCLUDED
#define TILE_FLOOR_H_INCLUDED

#include <Texture.h>
#include <VertexBuffer.h>
#include <Renderer.h>

class TileFloor
{
public:
    TileFloor();
    ~TileFloor();

    bool Init();

    void SetTexture(Texture* tex) { mTexture = tex; }
    void SetPosition(GameMath::Vec3 pos) { mPosition = pos; }
    void SetScale(float scale) { mScale = GameMath::Vec3(scale, scale, scale); }

    bool Draw(GameMath::Mat4& viewMat, Renderer& render);

private:
    VertexBuffer mVertBuf;
    static float sVertices[6*8]; // 2 triangles (3*2=6) * 8 floats per vertex
    static bool sInitted;
    GameMath::Vec3 mPosition; // top left coords
    GameMath::Vec3 mScale;
    Texture* mTexture;
};

#endif // TILE_FLOOR_H_INCLUDED

