#ifndef VERTEX_BUFFER_H_INCLUDED
#define VERTEX_BUFFER_H_INCLUDED

#include <stddef.h>
#include <string.h>
#include <rsxutil.h>

// forward declaration
class Renderer;

class VertexBuffer
{

friend class Renderer;

public:
    VertexBuffer();
    ~VertexBuffer();
    
    bool Init(
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indicesSize,
        size_t floatsPerVertex
    );
    
    void UpdateVertices(float* vertices, size_t verticesSize);

private:
    int* mIndices;
    size_t mIndicesSize;
    
    float* mVertices;
    size_t mVerticesSize;
    size_t mFloatsPerVertex;
};

#endif // VERTEX_BUFFER_H_INCLUDED
