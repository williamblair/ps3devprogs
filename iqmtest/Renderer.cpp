#include <Renderer.h>

#include "textured_shader_vpo.h"
#include "textured_shader_fpo.h"
//#include "dbg_nomtx_shader_vpo.h"
//#include "dbg_nomtx_shader_fpo.h"

Renderer::Renderer() :
    mHostAddr(nullptr),
    mContext(nullptr),
    mCurShader(nullptr)
{}

Renderer::~Renderer()
{}

bool Renderer::Init(int width, int height, const char* title)
{
    mHostAddr = memalign(HOST_ADDR_ALIGNMENT, HOSTBUFFER_SIZE);
    init_screen(mHostAddr, HOSTBUFFER_SIZE);
    mContext = context; // rsxutil.h
    
    SetDrawEnv();
    setRenderTarget(curr_fb);
    
    {
        rsxVertexProgram* vpo = (rsxVertexProgram*)textured_shader_vpo;
        rsxFragmentProgram* fpo = (rsxFragmentProgram*)textured_shader_fpo;
        //rsxVertexProgram* vpo = (rsxVertexProgram*)dbg_nomtx_shader_vpo;
        //rsxFragmentProgram* fpo = (rsxFragmentProgram*)dbg_nomtx_shader_fpo;
        mTexturedShader.Init(vpo, fpo);
    }
    
    // mat4s need to be transposed before being sent to shader
    mProjMat = GameMath::Transpose(GameMath::Perspective(
        GameMath::Deg2Rad(45.0f),
        (float)display_width / (float)display_height,
        1.0f, 1000.0f
    ));
    
    return true;
}


void Renderer::SetDrawEnv()
{
    rsxSetColorMask(
        mContext,
        GCM_COLOR_MASK_B | GCM_COLOR_MASK_G | GCM_COLOR_MASK_R | GCM_COLOR_MASK_A
    );
    rsxSetColorMaskMrt(mContext, 0);
    
    u16 x,y,w,h;
    f32 min, max;
    f32 scale[4], offset[4];
    
    x = 0;
    y = 0;
    w = display_width;
    h = display_height;
    min = 0.0f;
    max = 1.0f;
    scale[0] = w * 0.5f;
    scale[1] = h * -0.5f;
    scale[2] = (max - min) * 0.5f;
    scale[3] = 0.0f;
    
    offset[0] = x + w*0.5f;
    offset[1] = y + h*0.5f;
    offset[2] = (max + min) * 0.5f;
    offset[3] = 0.0f;
    
    rsxSetViewport(mContext, x,y,w,h, min,max, scale, offset);
    rsxSetScissor(mContext, x,y,w,h);
    
    rsxSetDepthTestEnable(mContext, GCM_TRUE);
    rsxSetDepthFunc(mContext, GCM_LESS);
    rsxSetShadeModel(mContext, GCM_SHADE_MODEL_SMOOTH);
    rsxSetDepthWriteEnable(mContext, 1);
    rsxSetFrontFace(mContext, GCM_FRONTFACE_CCW);
    rsxSetBlendEnable(mContext, GCM_TRUE);
    rsxSetBlendEquation(mContext, GCM_FUNC_ADD, GCM_FUNC_ADD);
    rsxSetBlendFunc(
        mContext,
        GCM_SRC_ALPHA,
        GCM_ONE_MINUS_SRC_ALPHA,
        GCM_SRC_ALPHA,
        GCM_ONE_MINUS_SRC_ALPHA
    );
}

void Renderer::Clear()
{
    //u32 color = 0; // black screen clear
    u32 color = 0xFFFFFFFF; // white screen clear
    SetDrawEnv();
    rsxSetClearColor(mContext, color);
    rsxSetClearDepthStencil(mContext, 0xFFFFFF00);
    rsxClearSurface(mContext,
        GCM_CLEAR_R | GCM_CLEAR_G | GCM_CLEAR_B | GCM_CLEAR_A |
        GCM_CLEAR_S | GCM_CLEAR_Z
    );
    rsxSetZMinMaxControl(mContext, 0,1,1);
    for (int i=0; i<8; ++i) {
        rsxSetViewportClip(mContext, i, display_width, display_height);
    }
}

void Renderer::Update()
{
    // rsxutil
    flip();
}

void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    VertexBuffer& vb)
{
    u32 offset = 0;
    
    // TODO - different shaders
    if (mCurShader != &mTexturedShader) {
        mTexturedShader.Use();
        mCurShader = &mTexturedShader;
    }
    mCurShader->SetVertexProgParam("projMatrix", (float*)mProjMat.v);
    // example has these transposed first
    GameMath::Mat4 modelView = GameMath::Transpose(viewMat * modelMat);
    mCurShader->SetVertexProgParam("modelViewMatrix", (float*)modelView.v);
    
    /*rsxSetUserClipPlaneControl(
        mContext,GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE
    );*/
    
    // position
    if (vb.mFloatsPerVertex >= 3)
    {
        rsxAddressToOffset(&vb.mVertices[0], &offset);
        rsxBindVertexArrayAttrib(
            mContext,               // context
            GCM_VERTEX_ATTRIB_POS, // u8 attr
            0, // ?
            offset, // u32 offset
            vb.mFloatsPerVertex * sizeof(float), // u8 stride
            3, // u8 elems
            GCM_VERTEX_DATA_TYPE_F32, // u8 dtype
            GCM_LOCATION_RSX // u8 location
        );
    }
    // normal
    if (vb.mFloatsPerVertex >= 6)
    {
        rsxAddressToOffset(&vb.mVertices[3], &offset);
        rsxBindVertexArrayAttrib(
            mContext,               // context
            GCM_VERTEX_ATTRIB_NORMAL, // u8 attr
            0, // ?
            offset, // u32 offset
            vb.mFloatsPerVertex * sizeof(float), // u8 stride
            3, // u8 elems
            GCM_VERTEX_DATA_TYPE_F32, // u8 dtype
            GCM_LOCATION_RSX // u8 location
        );
    }
    if (vb.mFloatsPerVertex >= 8)
    {
        rsxAddressToOffset(&vb.mVertices[6], &offset);
        rsxBindVertexArrayAttrib(
            mContext,               // context
            GCM_VERTEX_ATTRIB_TEX0, // u8 attr
            0, // ?
            offset, // u32 offset
            vb.mFloatsPerVertex * sizeof(float), // u8 stride
            2, // u8 elems
            GCM_VERTEX_DATA_TYPE_F32, // u8 dtype
            GCM_LOCATION_RSX // u8 location
        );
    }
    
    if (vb.mIndicesSize > 0)
    {
        rsxAddressToOffset(&vb.mIndices[0], &offset);
        rsxDrawIndexArray(
            mContext,
            GCM_TYPE_TRIANGLES, // u32 type
            offset, // u32 offset
            vb.mIndicesSize, // u32 count
            GCM_INDEX_TYPE_32B, // u32 data type
            GCM_LOCATION_RSX // u32 location
        );
    }
    else
    {
        //printf("rsx draw vertex array\n");
        // possibly todo - divide verticesSize by floatsPerVertex?
        // possibly, not sure...
        rsxDrawVertexArray(
            mContext,
            GCM_TYPE_TRIANGLES,
            0,
            vb.mVerticesSize/* / vb.mFloatsPerVertex*/
        );
    }
}

void Renderer::SetTexture(Texture& tex)
{
    u8 shaderTexUnit = 0;
    // default shader...
    if (mCurShader == nullptr) {
        mCurShader = &mTexturedShader;
        mCurShader->Use();
    }
    rsxProgramAttrib* fragmentAttrib =
        rsxFragmentProgramGetAttrib(mCurShader->mFpo, "texture");
    if (fragmentAttrib == nullptr) {
        printf("Renderer failed to get texture frag prog attrib\n");
        return;
    }
    shaderTexUnit = fragmentAttrib->index;
    
    u32 width = tex.mWidth;
    u32 height = tex.mHeight;
    u32 pitch = width * 4; // assumes 4 bytes per pixel
    
    rsxInvalidateTextureCache(mContext, GCM_INVALIDATE_TEXTURE);
    
    // TODO - can probably be done in Texture::Init
    gcmTexture& texture = tex.mTexture;
    texture.format = (GCM_TEXTURE_FORMAT_A8R8G8B8 | GCM_TEXTURE_FORMAT_LIN);
    texture.mipmap = 1;
    texture.dimension = GCM_TEXTURE_DIMS_2D;
    texture.cubemap = GCM_FALSE;
    texture.remap = 
        ((GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_B_SHIFT) |
       (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_G_SHIFT) |
       (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_R_SHIFT) |
       (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_A_SHIFT) |
       (GCM_TEXTURE_REMAP_COLOR_B << GCM_TEXTURE_REMAP_COLOR_B_SHIFT) |
       (GCM_TEXTURE_REMAP_COLOR_G << GCM_TEXTURE_REMAP_COLOR_G_SHIFT) |
       (GCM_TEXTURE_REMAP_COLOR_R << GCM_TEXTURE_REMAP_COLOR_R_SHIFT) |
       (GCM_TEXTURE_REMAP_COLOR_A << GCM_TEXTURE_REMAP_COLOR_A_SHIFT));
    texture.width = width;
    texture.height = height;
    texture.depth = 1;
    texture.location = GCM_LOCATION_RSX;
    texture.pitch = pitch;
    texture.offset = tex.mOffset;
    
    rsxLoadTexture(mContext,shaderTexUnit,&texture);
    rsxTextureControl(mContext,shaderTexUnit,GCM_TRUE,0<<8,12<<8,GCM_TEXTURE_MAX_ANISO_1);
    rsxTextureFilter(mContext,shaderTexUnit,0,GCM_TEXTURE_LINEAR,GCM_TEXTURE_LINEAR,GCM_TEXTURE_CONVOLUTION_QUINCUNX);
    rsxTextureWrapMode(mContext,shaderTexUnit,GCM_TEXTURE_REPEAT,GCM_TEXTURE_REPEAT,GCM_TEXTURE_REPEAT,0,GCM_TEXTURE_ZFUNC_LESS,0);
}

