#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <rsxutil.h>

// forward declaration
class Renderer;

class Texture
{
friend class Renderer;
    
public:
    Texture();
    ~Texture();
    
    bool Init(u8* pixelData, u32 width, u32 height, u32 bytesPerPixel);
    
    bool LoadTGA(const char* fileName);
    
private:
    u32* mBuffer;
    u32 mOffset;
    u32 mWidth;
    u32 mHeight;
    gcmTexture mTexture;
};

#endif // TEXTURE_H_INCLUDED
