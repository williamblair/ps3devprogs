#include <Input.h>

bool Input::sSysPadInitted = false;

Input::Input()
{
    mPadnum = 0;
    mIsActive = false;
    memset((void*)&mPaddata, 0, sizeof(mPaddata));
    memset((void*)&mPrevPadData, 0, sizeof(mPrevPadData));
    memset((void*)&mActparam, 0, sizeof(mActparam));
}

Input::~Input()
{
}

void Input::Update()
{
    ioPadGetInfo(&mPadInfo);
    if (mPadInfo.status[mPadnum])
    {
        mIsActive = true;
        memcpy((void*)&mPrevPadData, (void*)&mPaddata, sizeof(mPrevPadData));
        ioPadGetData(mPadnum, &mPaddata);
    }
    else
    {
        mIsActive = false;
    }
}

bool Input::IsHeld(Button b)
{
    if (!mIsActive) {
        return false;
    }
    bool held = false;
    switch (b)
    {
    case LEFT:     held = (bool)mPaddata.BTN_LEFT;     break;
    case DOWN:     held = (bool)mPaddata.BTN_DOWN;     break;
    case RIGHT:    held = (bool)mPaddata.BTN_RIGHT;    break;
    case UP:       held = (bool)mPaddata.BTN_UP;       break;
    case SQUARE:   held = (bool)mPaddata.BTN_SQUARE;   break;
    case CROSS:    held = (bool)mPaddata.BTN_CROSS;    break;
    case CIRCLE:   held = (bool)mPaddata.BTN_CIRCLE;   break;
    case TRIANGLE: held = (bool)mPaddata.BTN_TRIANGLE; break;
    default:
        break;
    }

    return held;
}

bool Input::IsClicked(Button b)
{
    if (!mIsActive) {
        return false;
    }
    bool clicked = false;
    switch (b)
    {
    case LEFT:     clicked = (!mPaddata.BTN_LEFT && mPrevPadData.BTN_LEFT);          break;
    case DOWN:     clicked = (!mPaddata.BTN_DOWN && mPrevPadData.BTN_DOWN);          break;
    case RIGHT:    clicked = (!mPaddata.BTN_RIGHT && mPrevPadData.BTN_RIGHT);        break;
    case UP:       clicked = (!mPaddata.BTN_UP && mPrevPadData.BTN_UP);              break;
    case SQUARE:   clicked = (!mPaddata.BTN_SQUARE && mPrevPadData.BTN_SQUARE);      break;
    case CROSS:    clicked = (!mPaddata.BTN_CROSS && mPrevPadData.BTN_CROSS);        break;
    case CIRCLE:   clicked = (!mPaddata.BTN_CIRCLE && mPrevPadData.BTN_CIRCLE);      break;
    case TRIANGLE: clicked = (!mPaddata.BTN_TRIANGLE && mPrevPadData.BTN_TRIANGLE);  break;
    default:
        break;
    }

    return clicked;
}

bool Input::SetSmallActuator(bool isOn)
{
    if (mIsActive)
    {
        mActparam.small_motor = (int)isOn;
        ioPadSetActDirect(mPadnum, &mActparam);
        return true;
    }
    return false;
}

bool Input::SetLargeActuator(unsigned char val)
{
    if (mIsActive)
    {
        mActparam.large_motor = val;
        ioPadSetActDirect(mPadnum, &mActparam);
        return true;
    }
    return false;
}


