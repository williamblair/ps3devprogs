#include <VertexBuffer.h>
#include <stdio.h>

VertexBuffer::VertexBuffer() :
    mIndices(nullptr),
    mIndicesSize(0),
    mVertices(nullptr),
    mVerticesSize(0),
    mFloatsPerVertex(0)
{}

VertexBuffer::~VertexBuffer()
{
    free(mVertices); mVertices = nullptr;
    free(mIndices); mIndices = nullptr;
}

bool VertexBuffer::Init(
    float* vertices,
    size_t verticesSize,
    int* indices,
    size_t indicesSize,
    size_t floatsPerVertex)
{
    mVertices = (float*)rsxMemalign(128, verticesSize * sizeof(float));
    if (!mVertices) { printf("Failed to alloc rsx vertices mem\n"); return false; }
    memcpy(mVertices, vertices, verticesSize * sizeof(float));
    mVerticesSize = verticesSize;
    mFloatsPerVertex = floatsPerVertex;
    
    if (indicesSize > 0) {
        mIndices = (int*)rsxMemalign(128, indicesSize * sizeof(int));
        if (!mIndices) { printf("Failed to alloc rsx indices mem\n"); return false; }
        memcpy(mIndices, indices, sizeof(int) * indicesSize);
        mIndicesSize = indicesSize;
    }
    
    return true;
}

void VertexBuffer::UpdateVertices(
    float* vertices,
    size_t verticesSize)
{
    if (verticesSize != mVerticesSize) {
        printf("ERROR - update vertices size != mVerticesSize\n");
        return;
    }
    memcpy(mVertices, vertices, verticesSize * sizeof(float));
}
