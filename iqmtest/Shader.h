#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

#include <rsxutil.h>

// forward declaration
class Renderer;

class Shader
{
friend class Renderer;
    
public:
    Shader();
    ~Shader();
    
    bool Init(rsxVertexProgram* vpo, rsxFragmentProgram* fpo);
    
    void Use() {
        // TODO - should get context from renderer...
        rsxLoadVertexProgram(context, mVpo, mVpUCode);
        rsxLoadFragmentProgramLocation(context, mFpo, mFpOffset, GCM_LOCATION_RSX);
    }
    
    void SetVertexProgParam(const char* name, float* val)
    {
        rsxProgramConst* prgCnst = rsxVertexProgramGetConst(mVpo, name);
        if (prgCnst == nullptr)
        {
            printf("Failed to get vertex prog const: %s\n", name);
            return;
        }
        // TODO - get context from Renderer
        rsxSetVertexProgramParameter(context, mVpo, prgCnst, val);
    }
    
private:
    rsxVertexProgram* mVpo;
    rsxFragmentProgram* mFpo;
    void* mVpUCode;
    void* mFpUCode;
    u32* mFpBuffer;
    u32 mFpOffset;
    
    bool InitUCode();
};

#endif // SHADER_H_INCLUDED
