// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "FPSActor.h"
#include "MoveComponent.h"

#define GL3_PROTOTYPES
#include <GL3/gl3.h>

#include <SDL/SDL_scancode.h>
//#include "Renderer.h"
//#include "AudioSystem.h"
//#include "Game.h"
//#include "AudioComponent.h"
#include "FPSCamera.h"
//#include "MeshComponent.h"
#include "Mesh.h"
#include "VertexArray.h"
//#include "PS3Input.h"
#include "Shader.h"
#include "Texture.h"

FPSActor::FPSActor()
{
	mMoveComp = new MoveComponent(this);
    // TODO
	//mAudioComp = new AudioComponent(this);
	mLastFootstep = 0.0f;
	//mFootstep = mAudioComp->PlayEvent("event:/Footstep");
	//mFootstep.SetPaused(true);

	mCameraComp = new FPSCamera(this);

	mFPSModel = new Actor();
	mFPSModel->SetScale(0.75f*0.25f);
}

bool FPSActor::LoadMesh()
{
	mMeshComp = new Mesh();
    if ( !mMeshComp->Load(ASSETS_DIR"/Assets/Rifle.gpmesh") ) {
        printf("failed to load rifle mesh\n");
        return false;
    }
    return true;
}

void FPSActor::UpdateActor(float deltaTime)
{
	Actor::UpdateActor(deltaTime);
    mMoveComp->Update(deltaTime);

	// Play the footstep if we're moving and haven't recently
	mLastFootstep -= deltaTime;
	if (fabsf(mMoveComp->GetForwardSpeed()) > 0.0001f && mLastFootstep <= 0.0f)
	{
		//mFootstep.SetPaused(false);
		//mFootstep.Restart();
		mLastFootstep = 0.5f;
	}
	
	// Update position of FPS model relative to actor position
    // Z negated due to desiring opposite positive z
	const Vector3 modelOffset(Vector3(2.0f, -2.0f, 2.0f));
	Vector3 modelPos = GetPosition();
	modelPos += GetForward() * (float)modelOffset.getZ();
	modelPos += GetRight() * (float)modelOffset.getX();
	modelPos.setY( modelPos.getY() + modelOffset.getY() );
	mFPSModel->SetPosition(modelPos);

	// Initialize rotation to actor rotation
	Quat q = GetRotation();
	// Rotate by pitch from camera
	//q = Quaternion::Concatenate(q, Quaternion(GetRight(), mCameraComp->GetPitch()));
    q = q * Quat::rotation(mCameraComp->GetPitch(), GetRight());
	mFPSModel->SetRotation(q);
    
    mCameraComp->Update(deltaTime);
}

void FPSActor::ActorInput(const uint8_t* keys, padData& pad)
{
	float forwardSpeed = 0.0f;
	float strafeSpeed = 0.0f;
	// wasd movement
	if (pad.BTN_UP)
	{
		forwardSpeed += 25.0f;
	}
	if (pad.BTN_DOWN)
	{
		forwardSpeed -= 25.0f;
	}
	if (pad.BTN_LEFT)
	{
		strafeSpeed -= 25.0f;
	}
	if (pad.BTN_RIGHT)
	{
		strafeSpeed += 25.0f;
	}

	mMoveComp->SetForwardSpeed(forwardSpeed);
	mMoveComp->SetStrafeSpeed(strafeSpeed);

	// Mouse movement
	// Get relative movement from SDL
	int x=0, y=0;
	//SDL_GetRelativeMouseState(&x, &y);
	// Assume mouse movement is usually between -500 and +500
	const int maxMouseSpeed = 500;
	// Rotation/sec at maximum speed
	const float maxAngularSpeed = M_PI * 8;
	float angularSpeed = 0.0f;
	if (x != 0)
	{
		// Convert to ~[-1.0, 1.0]
		angularSpeed = static_cast<float>(x) / maxMouseSpeed;
		// Multiply by rotation/sec
		angularSpeed *= maxAngularSpeed;
	}
	mMoveComp->SetAngularSpeed(angularSpeed);

	// Compute pitch
	const float maxPitchSpeed = M_PI * 8;
	float pitchSpeed = 0.0f;
	if (y != 0)
	{
		// Convert to ~[-1.0, 1.0]
		pitchSpeed = static_cast<float>(y) / maxMouseSpeed;
		pitchSpeed *= maxPitchSpeed;
	}
	mCameraComp->SetPitchSpeed(pitchSpeed);
}

void FPSActor::SetFootstepSurface(float value)
{
	// Pause here because the way I setup the parameter in FMOD
	// changing it will play a footstep
	//mFootstep.SetPaused(true);
	//mFootstep.SetParameter("Surface", value);
}

void FPSActor::SetVisible(bool visible)
{
	//mMeshComp->SetVisible(visible);
}

//void FPSActor::DrawActor(unsigned int transMatLoc, unsigned int normalMatLoc)
void FPSActor::DrawActor(Matrix4& projMat, Shader& shader)
{
    // TODO - not update here/hardcode time
    UpdateActor(1.0f/60.0f);
    VertexArray* va = mMeshComp->GetVertexArray();
    Texture* tex = mMeshComp->GetTexture(0);
    mFPSModel->Update(0.0f);
    const Matrix4& modelMat = mFPSModel->GetWorldTransform();
    
    Matrix4 viewProj = projMat * GetViewMatrix();

    shader.SetMatrixUniform("uViewProj", viewProj);
    shader.SetMatrixUniform("uWorldTransform", modelMat);
    
    tex->SetActive();
    va->Draw();
}
