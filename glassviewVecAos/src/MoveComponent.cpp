// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "Actor.h"
#include "MoveComponent.h"

MoveComponent::MoveComponent(Actor* owner) :
mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
,mOwner(owner)
{
	
}

void MoveComponent::Update(float deltaTime)
{
	if (fabsf(mAngularSpeed) > 0.0001f)
	{
		//Quaternion rot = mOwner->GetRotation();
        Quat rot = mOwner->GetRotation();
		float angle = mAngularSpeed * deltaTime;
		// Create quaternion for incremental rotation
		// (Rotate about up axis)
		//Quaternion inc(Vector3::UnitZ, angle);
        Quat inc = Quat::rotationY(angle);
		// Concatenate old and new quaternion
		//rot = Quaternion::Concatenate(rot, inc);
        rot = rot * inc;
		mOwner->SetRotation(rot);
	}
	
	if (fabsf(mForwardSpeed) > 0.0001f || fabsf(mStrafeSpeed) > 0.0001f)
	{
		Vector3 pos = mOwner->GetPosition();
		pos += mOwner->GetForward() * mForwardSpeed * deltaTime;
		pos += mOwner->GetRight() * mStrafeSpeed * deltaTime;
		mOwner->SetPosition(pos);
	}
}
