 // ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "Actor.h"
#include <algorithm>

Actor::Actor()
	:mState(EActive)
	,mPosition(0.0f,0.0f,0.0f)
	,mRotation(Quat::identity())
	,mScale(1.0f)
	,mRecomputeWorldTransform(true)
{
}

Actor::~Actor()
{
}

void Actor::Update(float deltaTime)
{
	if (mState == EActive)
	{
		ComputeWorldTransform();

		//UpdateComponents(deltaTime);
		UpdateActor(deltaTime);

		ComputeWorldTransform();
	}
}

void Actor::UpdateActor(float deltaTime)
{
}

void Actor::ProcessInput(const uint8_t* keyState, padData& pad)
{
	if (mState == EActive)
	{
		ActorInput(keyState, pad);
	}
}

void Actor::ActorInput(const uint8_t* keyState, padData& pad)
{
}

void Actor::ComputeWorldTransform()
{
	if (mRecomputeWorldTransform)
	{
		mRecomputeWorldTransform = false;
		// Scale, then rotate, then translate
		//mWorldTransform = Matrix4::scale(Vector3(mScale,mScale,mScale));
		//mWorldTransform *= Matrix4::rotation(mRotation);
		//mWorldTransform *= Matrix4::translation(mPosition);

        mWorldTransform = Matrix4::translation(mPosition) *
                          Matrix4::rotation(mRotation) *
                          Matrix4::scale(Vector3(mScale,mScale,mScale));

		// Inform components world transform updated
		//for (auto comp : mComponents)
		//{
		//	comp->OnUpdateWorldTransform();
		//}
	}
}
