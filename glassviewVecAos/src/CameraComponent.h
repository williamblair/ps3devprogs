// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include <vectormath/cpp/vectormath_aos.h>
using Matrix4 = Vectormath::Aos::Matrix4;

class CameraComponent
{
public:
	CameraComponent(class Actor* owner, int updateOrder = 200);
    virtual Matrix4& GetViewMatrix() = 0;
protected:
	void SetViewMatrix(const Matrix4& view);
    Actor* mOwner;
};
