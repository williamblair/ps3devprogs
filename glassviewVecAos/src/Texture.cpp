// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "Texture.h"
#include <EGL/egl.h>
#define GL3_PROTOTYPES
#include <GL3/gl3.h>
#include <GL3/rsxgl.h>
#include <GL3/rsxgl3ext.h>
#include <SDL/SDL_image.h>

Texture::Texture()
:mTextureID(0)
,mWidth(0)
,mHeight(0)
{
    
}

Texture::~Texture()
{
}

bool Texture::Load(const std::string& fileName)
{
    SDL_Surface* surface = nullptr;
    surface = IMG_Load( fileName.c_str() );
    if ( !surface ) {
        SDL_LogError(0, "Texture::Load: failed to load image: %s\n", IMG_GetError() );
        return false;
    }
    mWidth = surface->w;
    mHeight = surface->h;
    SDL_Log("Image width, height: %d, %d\n", mWidth, mHeight);

    SDL_LockSurface( surface );
    
    int format = GL_RGB;
    int bytesPerPx = surface->format->BytesPerPixel;
    SDL_Log( "BytesPerPx: %d", bytesPerPx );
    if ( bytesPerPx == 4 )
    {
        format = GL_RGBA;
    }
    // without swapping, pixel format is abgr... not sure if the reason is sdl or opengl
    unsigned char* flippedPixels = new unsigned char[mWidth * mHeight * bytesPerPx];
    if ( !flippedPixels ) {
        SDL_LogError(0, "Failed to alloc texture buff");
        SDL_UnlockSurface( surface );
        SDL_FreeSurface( surface );
        return false;
    }
    unsigned char* origPixels = (unsigned char*)surface->pixels;
    if ( bytesPerPx == 3 )
    {
        for ( int i = 0; i < mWidth*mHeight*bytesPerPx; i += 3)
        {
            flippedPixels[i+0] = origPixels[i+2];
            flippedPixels[i+1] = origPixels[i+1];
            flippedPixels[i+2] = origPixels[i+0];
        }
    }
    else if ( bytesPerPx == 4 )
    {
        for ( int i = 0; i < mWidth*mHeight*bytesPerPx; i += 4)
        {
            flippedPixels[i+0] = origPixels[i+3];
            flippedPixels[i+1] = origPixels[i+2];
            flippedPixels[i+2] = origPixels[i+1];
            flippedPixels[i+3] = origPixels[i+0];
        }
    }
    else
    {
        SDL_LogError(0, "Unsupported bytes per px: %d\n", bytesPerPx);
        SDL_UnlockSurface( surface );
        SDL_FreeSurface( surface );
        return false;
    }    

    glGenTextures( 1, &mTextureID );
    glBindTexture( GL_TEXTURE_2D, mTextureID );

    SDL_Log("Created Tex ID: %u\n", mTextureID );
    
    glTexImage2D( GL_TEXTURE_2D, 0, format, mWidth, mHeight, 0, format,
                  GL_UNSIGNED_BYTE, flippedPixels );
    
    SDL_UnlockSurface( surface );
    SDL_FreeSurface( surface );

    delete[] flippedPixels;
    
    // Enable linear filtering
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    
    return true;
}

void Texture::Unload()
{
    glDeleteTextures( 1, &mTextureID );
}

void Texture::SetActive()
{
    glBindTexture( GL_TEXTURE_2D, mTextureID );
}

