// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include <vector>
#include <cstdint>
#include <SDL/SDL.h>
#include <io/pad.h>
#include <vectormath/cpp/vectormath_aos.h>
//using namespace Vectormath::Aos;
using Vector3 = Vectormath::Aos::Vector3;
using Quat = Vectormath::Aos::Quat;
using Matrix4 = Vectormath::Aos::Matrix4;
using Point3 = Vectormath::Aos::Point3;

class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};

	Actor();
	virtual ~Actor();

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);

	// ProcessInput function called from Game (not overridable)
	void ProcessInput(const uint8_t* keyState, padData& pad);
	// Any actor-specific input code (overridable)
	virtual void ActorInput(const uint8_t* keyState, padData& pad);

	// Getters/setters
	const Vector3& GetPosition() const { return mPosition; }
	void SetPosition(const Vector3& pos) { mPosition = pos; mRecomputeWorldTransform = true; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale;  mRecomputeWorldTransform = true; }
	const Quat& GetRotation() const { return mRotation; }
	void SetRotation(const Quat& rotation) { mRotation = rotation;  mRecomputeWorldTransform = true; }
	
	void ComputeWorldTransform();
	const Matrix4& GetWorldTransform() const { return mWorldTransform; }

	//Vector3 GetForward() const { return Vector3::Transform(Vector3::UnitX, mRotation); }
    Vector3 GetForward() const { return rotate(mRotation, Vector3(0.0f, 0.0f, -1.0f)); }
	//Vector3 GetRight() const { return Vector3::Transform(Vector3::UnitY, mRotation); }
    Vector3 GetRight() const { return rotate(mRotation, Vector3(1.0f, 0.0f, 0.0f)); }

	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }

private:
	// Actor's state
	State mState;

	// Transform
	Matrix4 mWorldTransform;
	Vector3 mPosition;
	Quat mRotation;
	float mScale;
	bool mRecomputeWorldTransform;
};
