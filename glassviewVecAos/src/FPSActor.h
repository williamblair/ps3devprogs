// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include "Actor.h"
#include "FPSCamera.h"
// TODO
//#include "SoundEvent.h"

class FPSActor : public Actor
{
public:
	FPSActor();

    bool LoadMesh();

	void UpdateActor(float deltaTime) override;
	void ActorInput(const uint8_t* keys, padData& pad) override;

	void SetFootstepSurface(float value);

	void SetVisible(bool visible);
    
    Matrix4& GetViewMatrix() { return mCameraComp->GetViewMatrix(); }
    
    void DrawActor(Matrix4& projMat, class Shader& shader);
    
private:
	class MoveComponent* mMoveComp;
    // TODO
	//class AudioComponent* mAudioComp;
	class Mesh* mMeshComp;
	class FPSCamera* mCameraComp;
	class Actor* mFPSModel;
    // TODO
	//SoundEvent mFootstep;
	float mLastFootstep;
};
