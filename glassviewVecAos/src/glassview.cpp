/*
 * glassview - render models loaded with the assimp asset importer.
 */

#include <string>
#include <cstring>
#include <cassert>
#include <vector>

#define GL3_PROTOTYPES
#include <GL3/gl3.h>

#include <SDL/SDL_image.h>

#include "rsxgltest.h"
#include "sine_wave.h"
#include "VertexArray.h"
#include "FPSActor.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"

#include <vectormath/cpp/vectormath_aos.h>
using Vectormath::Aos::Matrix4;
using Vectormath::Aos::Vector3;
using Vectormath::Aos::Point3;
using Vectormath::Aos::Quat;

#include <stddef.h>


#include <io/pad.h>

#include <math.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "glassimp.h"

// degrees per second:
#define DTOR(X) ((X)*0.01745329f) // degrees to radians
#define RTOD(d) ((d)*57.295788f) // radians to degrees

static FPSActor fpsActor;
static Actor sphereActor;
static Actor cubeActor;
#define NUM_FLOOR (10*10)
static Actor floorActors[NUM_FLOOR];
static Mesh fpsMesh;
static Mesh cubeMesh;
static Mesh sphereMesh;
static Mesh floorMesh; // single floor mesh shared by floorActors
static Shader shader;
static Texture defaultTexture;

const char * rsxgltest_name = "glassview";

struct sine_wave_t rgb_waves[3] = {
    { 0.5f,
        0.5f,
        1.0f
    },
    {
        0.5f,
        0.5f,
        1.5f
    },
    {
        0.5f,
        0.5f,
        2.5f
    }
};

const GLfloat light[4] = { 10.0f, 10.0f, 10.0f, 1.0f };

Matrix4 ProjMatrix = Matrix4::perspective(DTOR(70.0f), 1920.0f/1080.0f, 0.1f, 10000.0f);

extern "C"
void
rsxgltest_pad(unsigned int,const padData * paddata)
{
    fpsActor.ActorInput(nullptr, *(padData*)paddata);
}

extern "C"
void
rsxgltest_init(int argc,const char ** argv)
{
    // Init SDL image
    {
        int flags = IMG_INIT_JPG | IMG_INIT_PNG;
        int initted = IMG_Init(flags);
        if ((initted & flags) != flags) {
            printf("IMG_Init: Failed to init required jpg and png support!\n");
            printf("IMG_Init: %s\n", IMG_GetError());
            exit(EXIT_FAILURE);
        }
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // Set up us the program:
    if ( !shader.Load(ASSETS_DIR"/shaders/Phong.vert",
                      ASSETS_DIR"/shaders/Phong.frag") )
    {
        printf("Failed to load shader\n");
        exit(EXIT_FAILURE);
    }
    shader.SetActive();

    shader.SetMatrixUniform("uViewProj", ProjMatrix);
    
    // Set lighting uniforms 
    shader.SetFloatUniform("uSpecPower", 100.0f);
    shader.SetVectorUniform("uAmbientLight", Vector3(0.2f, 0.2f, 0.2f));
    shader.SetVectorUniform("uDirLight.mDirection", Vector3(0.0f, -0.707f, -0.707f));
    shader.SetVectorUniform("uDirLight.mDiffuseColor", Vector3(0.78f, 0.88f, 1.0f));
    shader.SetVectorUniform("uDirLight.mSpecColor", Vector3(0.8f, 0.8f, 0.8f));
    
    // only one texture used
    shader.SetIntUniform("uTexture", 0); // GL_TEXTURE0

#if 0
    // Draw wireframe models for now:
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glLineWidth(1.0);
#endif

    // The hell with that, draw a diffuse-shaded object, dammit:
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    
    fpsActor.SetPosition(Vector3(0.0f,0.0f,11.525f));
    fpsActor.UpdateActor(0.0f);
    
    if ( !fpsMesh.Load(ASSETS_DIR"/Assets/Rifle.gpmesh") ) {
        exit(EXIT_FAILURE);
    }
    
    if ( !sphereMesh.Load(ASSETS_DIR"/Assets/Sphere.gpmesh") ||
         !cubeMesh.Load(ASSETS_DIR"/Assets/Cube.gpmesh")) {
        exit(EXIT_FAILURE);
    }
    sphereActor.SetPosition(Vector3(-5.0f, 0.0f, -20.0f));
    sphereActor.SetScale(0.10f);
    cubeActor.SetPosition(Vector3(5.0f, 0.0f, -20.0f));
    cubeActor.SetScale(1.0f);

    if ( !floorMesh.Load(ASSETS_DIR"/Assets/Plane.gpmesh") ) {
        exit(EXIT_FAILURE);
    }
    {
        const float scale = 0.05f;
        const float start = -1250.0f * scale;
        const float size = 250.0f * scale;
        int actorIndex = 0;
        for (int i = 0; i < 10; ++i)
        {
            for (int j = 0; j < 10; ++j)
            {
                Actor* a = &floorActors[actorIndex++];
                a->SetPosition(Vector3(start + i*size, -100.0f * scale, -(start + j*size)));
                a->SetScale(0.25f);
            }
        }
    }
    
    if ( !fpsActor.LoadMesh() ) {
        exit(EXIT_FAILURE);
    }
    
    if ( !defaultTexture.Load(ASSETS_DIR"/Assets/Default.png") ) {
        exit(EXIT_FAILURE);
    }
}

extern "C"
int
rsxgltest_draw()
{
    fpsActor.UpdateActor(0.0f);
    Matrix4& ViewTransform = fpsActor.GetViewMatrix();
    Matrix4 ViewProj = ProjMatrix * ViewTransform;
    shader.SetMatrixUniform("uViewProj", ViewProj);
    
    Vector3 cameraPos = fpsActor.GetPosition();
    shader.SetVectorUniform("uCameraPos", cameraPos);
    
    float rgb[3] = {
        compute_sine_wave(rgb_waves,rsxgltest_elapsed_time),
        compute_sine_wave(rgb_waves + 1,rsxgltest_elapsed_time),
        compute_sine_wave(rgb_waves + 2,rsxgltest_elapsed_time)
    };

    glClearColor(rgb[0] * 0.1,rgb[1] * 0.1,rgb[2] * 0.1,1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /*{
        Quat pitchQuat = Quat::rotationX(DTOR(model_pitch));
        Quat yawQuat = Quat::rotationY(DTOR(model_yaw));
        Quat totalRot = pitchQuat * yawQuat;
        cubeActor.SetRotation( totalRot );
    }*/
    cubeActor.UpdateActor(1.0f/60.0f);
    cubeActor.Update(0.0f);
    Matrix4 modelView = cubeActor.GetWorldTransform();
    shader.SetMatrixUniform("uWorldTransform", modelView);
    shader.SetIntUniform("uTexture", 0); // GL_TEXTURE0
    cubeMesh.GetTexture(0)->SetActive();
    cubeMesh.GetVertexArray()->Draw();

    /*{
        Quat pitchQuat = Quat::rotationX(DTOR(model_pitch));
        Quat yawQuat = Quat::rotationY(DTOR(model_yaw));
        Quat totalRot = pitchQuat * yawQuat;
        sphereActor.SetRotation( totalRot );
    }*/
    sphereActor.UpdateActor(1.0f/60.0f);
    sphereActor.Update(0.0f);
    modelView = sphereActor.GetWorldTransform();
    shader.SetMatrixUniform("uWorldTransform", modelView);
    shader.SetIntUniform("uTexture", 0); // GL_TEXTURE0
    sphereMesh.GetTexture(0)->SetActive();
    sphereMesh.GetVertexArray()->Draw();

    for (int i=0; i<NUM_FLOOR; ++i)
    {
        Actor* a = &floorActors[i];
        a->UpdateActor(1.0f/60.0f);
        a->Update(0.0f);
        Matrix4 modelView = a->GetWorldTransform();
        shader.SetMatrixUniform("uWorldTransform", modelView);
        shader.SetIntUniform("uTexture", 0);
        
        floorMesh.GetTexture(0)->SetActive();
        floorMesh.GetVertexArray()->Draw();
    }
    
    fpsActor.DrawActor(ProjMatrix, shader);
    glFlush();

    return 1;
}

extern "C"
void
rsxgltest_exit()
{
}
