#include <VertexBuffer.h>
#include <stdio.h>

VertexBuffer::VertexBuffer() :
    mIndices(nullptr),
    mIndicesSize(0),
    mVertices(nullptr),
    mVerticesSize(0),
    mType(UNINITIALIZED)
{}

VertexBuffer::~VertexBuffer()
{
    delete[] mVertices; mVertices = nullptr;
    delete[] mIndices; mIndices = nullptr;
}

bool VertexBuffer::Init(
    Type type,
    float* vertices,
    size_t verticesSize,
    int* indices,
    size_t indicesSize)
{
    mType = type;
    mVertices = new float[verticesSize];
    if (!mVertices) { printf("Failed to alloc vertices mem\n"); return false; }
    memcpy(mVertices, vertices, verticesSize * sizeof(float));
    mVerticesSize = verticesSize;
    
    if (indicesSize > 0) {
        mIndices = new int[indicesSize];
        if (!mIndices) { printf("Failed to alloc indices mem\n"); return false; }
        memcpy(mIndices, indices, sizeof(int) * indicesSize);
        mIndicesSize = indicesSize;
    }
    
    return true;
}

void VertexBuffer::UpdateVertices(
    float* vertices,
    size_t verticesSize)
{
    if (verticesSize != mVerticesSize) {
        printf("ERROR - update vertices size != mVerticesSize\n");
        return;
    }
    memcpy(mVertices, vertices, verticesSize * sizeof(float));
}

