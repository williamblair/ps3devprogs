#include <stdio.h>

//#include <tiny3d.h>
//#include <matrix.h>
//#include <gamemath/ps3/GameMath.h>
#include <Renderer.h>
#include <Camera.h>
#include <Input.h>
#include <Texture.h>
#include <IqmMesh.h>

using namespace GameMath;

static FPSCamera gCamera;
static Input gInput;
static Renderer gRender;
static Texture gTexture;
static IqmMesh gIqm;

void MoveCamera(const float dt)
{
    MoveComponent& mc = gCamera.GetMoveComponent();
    const float moveSpeed = 5.0f;
    if (gInput.GetLeftAnalogY() > 200) {
        mc.MoveForward(-moveSpeed * dt);
    }
    else if (gInput.GetLeftAnalogY() < 50) {
        mc.MoveForward(moveSpeed * dt);
    }

    if (gInput.GetLeftAnalogX() > 200) {
        mc.MoveRight(moveSpeed * dt);
    }
    else if (gInput.GetLeftAnalogX() < 50) {
        mc.MoveRight(-moveSpeed * dt);
    }

    const float rotSpeed = 30.0f;
    if (gInput.GetRightAnalogY() > 200) {
        mc.AddPitch(-rotSpeed * dt);
    }
    else if (gInput.GetRightAnalogY() < 50) {
        mc.AddPitch(rotSpeed * dt);
    }
    if (gInput.GetRightAnalogX() > 200) {
        mc.AddYaw(-rotSpeed * dt);
    }
    else if (gInput.GetRightAnalogX() < 50) {
        mc.AddYaw(rotSpeed * dt);
    }
}

int main()
{
    Mat4 modelMat = GameMath::Scale(0.1f, 0.1f, 0.1f);

    // 4MB vertex data
    if (!gRender.Init(1024, 768, "Unused Title!")) {
        printf("Error init tiny3D\n");
        return 1;
    }
    if (!gInput.Init(0)) {
        printf("Error gInput init\n");
        return 1;
    }
    if (!gTexture.LoadTGA(ASSETS_DIR"/gina.tga")) {
        printf("Error gTexture init\n");
        return 1;
    }
    if (!gIqm.Init(ASSETS_DIR"/gina.iqm")) {
        printf("Error gIqm init\n");
        return 1;
    }
    {
        IqmMesh::Anim idleAnim = { 0, 180, "idle" };
        IqmMesh::Anim walkAnim = { 181, 181+30-1, "walk" };
        gIqm.AddAnim(idleAnim);
        gIqm.AddAnim(walkAnim);
        gIqm.SetAnim("idle");
    }

    gCamera.GetMoveComponent().position = Vec3(0.0f, 5.0f, 20.0f);
    gCamera.Update();

    //gIqm.Update((1.0f/60.0f) * 30.0f);

    for (;;)
    {
        gRender.Clear();
        gInput.Update();

        // TODO - timer
        MoveCamera(1.0f/60.0f);
        gCamera.Update();

        gIqm.Update((1.0f/60.0f) * 30.0f);

        gRender.SetTexture(gTexture);
        gIqm.DrawVertexBuffer(modelMat, gCamera.GetViewMat(), gRender, 0);

        gRender.Update();
    }

    return 0;
}

