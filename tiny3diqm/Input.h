#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

class Input
{
public:

    Input();
    ~Input();

    enum Button
    {
        LEFT = 0,
        DOWN,
        RIGHT,
        UP,
        SQUARE,
        CROSS,
        CIRCLE,
        TRIANGLE
    };

    bool Init(int padnum)
    {
        if (!sSysPadInitted) {
            // TODO - look into this function
            ioPadInit(7);
            sSysPadInitted = true;
        }
        mPadnum = padnum;
        return true;
    }

    void Update();
    bool IsHeld(Button b);
    bool IsClicked(Button b);
    unsigned short GetLeftAnalogX() { return mPaddata.ANA_L_H; }
    unsigned short GetLeftAnalogY() { return mPaddata.ANA_L_V; }
    unsigned short GetRightAnalogX() { return mPaddata.ANA_R_H; }
    unsigned short GetRightAnalogY() { return mPaddata.ANA_R_V; }

    bool SetSmallActuator(bool isOn); // either on or off
    bool SetLargeActuator(unsigned char val); // 0-255

private:
    static bool sSysPadInitted;
    padInfo mPadInfo;
    padData mPaddata;
    padData mPrevPadData;
    bool mIsActive;
    padActParam mActparam;
    int mPadnum;
};

#endif // INPUT_H_INCLUDED


