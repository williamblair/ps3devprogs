#ifndef VERTEX_BUFFER_H_INCLUDED
#define VERTEX_BUFFER_H_INCLUDED

#include <stddef.h>
#include <string.h>

// forward declaration
class Renderer;

class VertexBuffer
{

friend class Renderer;

public:
    VertexBuffer();
    ~VertexBuffer();

    enum Type
    {
        POS_COLOR,
        POS_TEX,
        UNINITIALIZED
    };
    
    bool Init(
        Type type,
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indicesSize
    );
    
    void UpdateVertices(float* vertices, size_t verticesSize);

private:
    int* mIndices;
    size_t mIndicesSize;
    
    float* mVertices;
    size_t mVerticesSize;
    
    Type mType;
};

#endif // VERTEX_BUFFER_H_INCLUDED
