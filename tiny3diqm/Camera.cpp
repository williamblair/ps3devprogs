#include <Camera.h>

FPSCamera::FPSCamera()
{
    mViewMat = GameMath::LookAt(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

FPSCamera::~FPSCamera()
{}

void FPSCamera::Update()
{
    mMoveComponent.Update();
    
    mViewMat = GameMath::LookAt(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

