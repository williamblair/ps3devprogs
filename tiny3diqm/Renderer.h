#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <tiny3d.h>

#include <VertexBuffer.h>
#include <Texture.h>

//#include <gamemath/ps3/GameMath.h>
#include <gamemath/cpp/GameMath.h>

class Renderer
{
public:
    Renderer();
    ~Renderer();
    
    bool Init(int width, int height, const char* title);
    
    void Clear();
    void Update();
    
    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        VertexBuffer& vb
    );
    
    void SetTexture(Texture& tex);
    
private:
    
    GameMath::Mat4 mProjMat;
    GameMath::Mat4 mProjMatTiny3d;

    // tiny3d uses left-handed (+x=right, +y=up, z=away from player)
    // GameMath uses right-handed (+x=right, +y=up, z=towards player)
    inline GameMath::Mat4 gameMath2TinyProjMat(GameMath::Mat4& mat) {
        GameMath::Mat4 result = Transpose(mat);
        result.v[1][1] = -result.v[1][1];
        result.v[2][2] = -result.v[2][2];
        result.v[3][2] = -result.v[3][2];
        result.v[2][3] = -result.v[2][3] * 0.5f;
        return result;
    }
    inline GameMath::Mat4 gameMath2TinyModelViewMat(GameMath::Mat4& mat) {
        GameMath::Mat4 result = mat;
        result.v[3][2] = -result.v[3][2];
        return result;
    }
};

#endif // RENDERER_H_INCLUDED
