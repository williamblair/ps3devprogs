#include <Renderer.h>
#include <stdio.h>

Renderer::Renderer()
{}

Renderer::~Renderer()
{}

bool Renderer::Init(int width, int height, const char* title)
{
    (void)width;
    (void)height;
    (void)title;

    // 4MB vertex data
    if (tiny3d_Init(TINY3D_Z16 | 4*1024*1024) < 0) {
        printf("Error init tiny3D\n");
        return false;
    }

    mProjMat = GameMath::Perspective(
        GameMath::Deg2Rad(60.0f), // fov radians
        640.0f/480.0f, // aspect
        0.00125f, // near
        300.0f // far
    );
    mProjMatTiny3d = gameMath2TinyProjMat(mProjMat);

    return true;
}

void Renderer::Clear()
{
    // alpha is first in color (argb)
    // TODO - set bg color
    tiny3d_Clear(0xFF000000, TINY3D_CLEAR_ALL);

    // enable alpha test and alpha blending
    blend_src_func srcFun = (blend_src_func)(TINY3D_BLEND_FUNC_SRC_RGB_SRC_ALPHA | TINY3D_BLEND_FUNC_SRC_ALPHA_SRC_ALPHA);
    blend_dst_func dstFun = (blend_dst_func)(TINY3D_BLEND_FUNC_DST_RGB_ONE_MINUS_SRC_ALPHA | TINY3D_BLEND_FUNC_DST_ALPHA_ZERO);
    blend_func blndFun = (blend_func)(TINY3D_BLEND_RGB_FUNC_ADD | TINY3D_BLEND_ALPHA_FUNC_ADD);
    tiny3d_AlphaTest(1, 0x10, TINY3D_ALPHA_FUNC_GEQUAL);
    tiny3d_BlendFunc(1, srcFun, dstFun, blndFun);

    // Set 3d mode
    tiny3d_Project3D();
}

void Renderer::Update()
{
    tiny3d_Flip();
}

void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    VertexBuffer& vb)
{
    // Set model,view,projection matrix
    GameMath::Mat4 modelView = viewMat * modelMat;
    GameMath::Mat4 modelViewTiny3d = gameMath2TinyModelViewMat(modelView);
    tiny3d_SetProjectionMatrix((MATRIX*)&mProjMatTiny3d);
    tiny3d_SetMatrixModelView((MATRIX*)&modelViewTiny3d);

    // Set polygon type
    tiny3d_SetPolygon(TINY3D_TRIANGLES);

    // Draw vertices
    if (vb.mIndicesSize > 0)
    {
        switch (vb.mType)
        {
        case VertexBuffer::POS_COLOR:
        {
            const size_t floatsPerVertex = 6; // x,y,z,r,g,b
            for (size_t i=0; i<vb.mIndicesSize; i+=3)
            {
                const size_t vertIndex1 = vb.mIndices[i+0] * floatsPerVertex;
                const size_t vertIndex2 = vb.mIndices[i+1] * floatsPerVertex;
                const size_t vertIndex3 = vb.mIndices[i+2] * floatsPerVertex;

                // position
                const float* pos1 = &vb.mVertices[vertIndex1+0];
                const float* pos2 = &vb.mVertices[vertIndex2+0];
                const float* pos3 = &vb.mVertices[vertIndex3+0];

                // color
                const float* color1 = &vb.mVertices[vertIndex1+3]; // skip position
                const float* color2 = &vb.mVertices[vertIndex2+3];
                const float* color3 = &vb.mVertices[vertIndex3+3];
                
                tiny3d_VertexPos(pos1[0], pos1[1], pos1[2]);
                tiny3d_VertexFcolor(color1[0], color1[1], color1[2], 1.0f);
                tiny3d_VertexPos(pos2[0], pos2[1], pos2[2]);
                tiny3d_VertexFcolor(color2[0], color2[1], color2[2], 1.0f);
                tiny3d_VertexPos(pos3[0], pos3[1], pos3[2]);
                tiny3d_VertexFcolor(color3[0], color3[1], color3[2], 1.0f);
            }
            break;
        }
        case VertexBuffer::POS_TEX:
        {
            const size_t floatsPerVertex = 5; // x,y,z,u,v
            for (size_t i=0; i<vb.mIndicesSize; i+=3)
            {
                const size_t vertIndex1 = vb.mIndices[i+0] * floatsPerVertex;
                const size_t vertIndex2 = vb.mIndices[i+1] * floatsPerVertex;
                const size_t vertIndex3 = vb.mIndices[i+2] * floatsPerVertex;

                // position
                const float* pos1 = &vb.mVertices[vertIndex1+0];
                const float* pos2 = &vb.mVertices[vertIndex2+0];
                const float* pos3 = &vb.mVertices[vertIndex3+0];

                // tex coord
                const float* uv1 = &vb.mVertices[vertIndex1+3]; // skip position
                const float* uv2 = &vb.mVertices[vertIndex2+3];
                const float* uv3 = &vb.mVertices[vertIndex3+3];
                
                tiny3d_VertexPos(pos1[0], pos1[1], pos1[2]);
                tiny3d_VertexTexture(uv1[0], uv1[1]);
                tiny3d_VertexPos(pos2[0], pos2[1], pos2[2]);
                tiny3d_VertexTexture(uv2[0], uv2[1]);
                tiny3d_VertexPos(pos3[0], pos3[1], pos3[2]);
                tiny3d_VertexTexture(uv3[0], uv3[1]);
            }
            break;
        }
        default:
            printf("ERROR - unhandled vertexbuffer type\n");
            break;
        }
    }
    else
    {
        switch (vb.mType)
        {
        case VertexBuffer::POS_COLOR:
        {
            const size_t floatsPerVertex = 6; // x,y,z,r,g,b
            for (size_t i=0; i<vb.mVerticesSize; i+=floatsPerVertex*3)
            {
                // position
                const float* pos1 = &vb.mVertices[i + floatsPerVertex*0 + 0];
                const float* pos2 = &vb.mVertices[i + floatsPerVertex*1 + 0];
                const float* pos3 = &vb.mVertices[i + floatsPerVertex*2 + 0];

                // color
                const float* color1 = &vb.mVertices[i + floatsPerVertex*0 + 3]; // skip position
                const float* color2 = &vb.mVertices[i + floatsPerVertex*1 + 3];
                const float* color3 = &vb.mVertices[i + floatsPerVertex*2 + 3];
                
                tiny3d_VertexPos(pos1[0], pos1[1], pos1[2]);
                tiny3d_VertexFcolor(color1[0], color1[1], color1[2], 1.0f);
                tiny3d_VertexPos(pos2[0], pos2[1], pos2[2]);
                tiny3d_VertexFcolor(color2[0], color2[1], color2[2], 1.0f);
                tiny3d_VertexPos(pos3[0], pos3[1], pos3[2]);
                tiny3d_VertexFcolor(color3[0], color3[1], color3[2], 1.0f);
            }
            break;
        }
        case VertexBuffer::POS_TEX:
        {
            const size_t floatsPerVertex = 5; // x,y,z,u,v
            for (size_t i=0; i<vb.mVerticesSize; i+=floatsPerVertex*3)
            {
                // position
                const float* pos1 = &vb.mVertices[i + floatsPerVertex*0 + 0];
                const float* pos2 = &vb.mVertices[i + floatsPerVertex*1 + 0];
                const float* pos3 = &vb.mVertices[i + floatsPerVertex*2 + 0];

                // tex coord
                const float* uv1 = &vb.mVertices[i + floatsPerVertex*0 + 3]; // skip position
                const float* uv2 = &vb.mVertices[i + floatsPerVertex*1 + 3];
                const float* uv3 = &vb.mVertices[i + floatsPerVertex*2 + 3];
                
                tiny3d_VertexPos(pos1[0], pos1[1], pos1[2]);
                tiny3d_VertexTexture(uv1[0], uv1[1]);
                tiny3d_VertexPos(pos2[0], pos2[1], pos2[2]);
                tiny3d_VertexTexture(uv2[0], uv2[1]);
                tiny3d_VertexPos(pos3[0], pos3[1], pos3[2]);
                tiny3d_VertexTexture(uv3[0], uv3[1]);
            }
            break;
        }
        default:
            printf("ERROR - unhandled vertexbuffer type\n");
            break;
        }
    }

    tiny3d_End();

    // here projection, modelview, texture can be changed and tiny3d_setpolygon can
    // be called again to start drawing more vertices
}

void Renderer::SetTexture(Texture& tex)
{
    tiny3d_SetTexture(
        0, // Unit = 0 for TEX0, 1 for TEX1, ...
        tex.mRsxTexOffset, // rsx texture offset
        tex.mWidth, tex.mHeight, // image width, height
        tex.mWidth*4, // stride = width*4 for 32bit color
        TINY3D_TEX_FORMAT_A8R8G8B8, // argb color
        0 // smooth = 1 or 0
    );
}

