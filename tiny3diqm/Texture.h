#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <tiny3d.h>

// forward declaration
class Renderer;

class Texture
{
friend class Renderer;
    
public:
    Texture();
    ~Texture();
    
    bool Init(u8* pixelData, u32 width, u32 height, u32 bytesPerPixel);
    
    bool LoadTGA(const char* fileName);
    
private:
    void* mRsxMem;
    u32 mRsxTexOffset;
    u32 mWidth;
    u32 mHeight;
};

#endif // TEXTURE_H_INCLUDED
