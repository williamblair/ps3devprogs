#ifndef __RSXGL_RENDERER_H_INCLUDED__
#define __RSXGL_RENDERER_H_INCLUDED__

#include <EGL/egl.h>
#define GL3_PROTOTYPES
#include <GL3/gl3.h>
#include <GL3/rsxgl.h>
#include <GL3/rsxgl3ext.h>

#include "rsxgl_config.h"
#include <rsx/commands.h>

class Renderer
{
public:

    Renderer();
    ~Renderer();

    bool Init();
    void Shutdown();

    EGLDisplay& GetDisplay() { return mDisplay; }
    EGLSurface& GetSurface() { return mSurface; }
    EGLContext& GetContext() { return mCtx; }

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

    inline EGLBoolean Update()
    {
        return eglSwapBuffers( mDisplay, mSurface );
    }

private:

    EGLDisplay mDisplay;
    EGLSurface mSurface;
    EGLContext mCtx;

    int mWidth;
    int mHeight;
};

#endif // __RSXGL_RENDERER_H_INCLUDED__

