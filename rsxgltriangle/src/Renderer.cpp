#include "Renderer.h"
#include <stdio.h>
#include <sysutil/sysutil.h>

Renderer::Renderer()
{}

Renderer::~Renderer()
{}

bool Renderer::Init()
{
    mDisplay = eglGetDisplay( EGL_DEFAULT_DISPLAY );
    if ( mDisplay == EGL_NO_DISPLAY )
    {
        printf( "eglGetDisplay failed: %x\n", eglGetError() );
        return false;
    }

    EGLint version0 = 0;
    EGLint version1 = 0;
    EGLBoolean result = eglInitialize( mDisplay, &version0, &version1 );
    if ( !result )
    {
        printf( "eglInitialize failed: %x\n", eglGetError() );
        return false;
    }

    printf( "eglInitialize version: %i %i:%i\n", version0, version1, (int)result );
    EGLint attribs[] = {
        EGL_RED_SIZE,8,
        EGL_BLUE_SIZE,8,
        EGL_GREEN_SIZE,8,
        EGL_ALPHA_SIZE,8,

        EGL_DEPTH_SIZE,16,
        EGL_NONE
    };
    EGLConfig config;
    EGLint nconfig = 0;
    result = eglChooseConfig( mDisplay, attribs, &config, 1, &nconfig );
    printf( "eglChooseConfig:%i %u configs\n",(int)result,nconfig) ;
    if ( nconfig <= 0 )
    {
        printf( "egl nconfig <=0\n" );
        result = eglTerminate( mDisplay );
        return false;
    }

    mSurface = eglCreateWindowSurface( mDisplay, config, 0, 0 );
    if ( mSurface == EGL_NO_SURFACE )
    {
        printf( "eglCreateWindowSurface failed: %x\n",eglGetError() );
        result = eglTerminate( mDisplay );
        return false;
    }

    eglQuerySurface( mDisplay, mSurface, EGL_WIDTH, &mWidth );
    eglQuerySurface( mDisplay, mSurface, EGL_HEIGHT, &mHeight );

    printf( "eglCreateWindowSurface: %ix%i\n", mWidth, mHeight );
    
    mCtx = eglCreateContext( mDisplay, config, 0, 0 );
    printf( "eglCreateContext: %lu\n", (unsigned long)mCtx );

    if ( mCtx == EGL_NO_CONTEXT )
    {
        printf( "eglCreateContext failed: %x\n", eglGetError() );
        result = eglTerminate( mDisplay );
        return false;
    }

    result = eglMakeCurrent( mDisplay, mSurface, mSurface, mCtx );
    if ( result != EGL_TRUE )
    {
        printf( "eglMakeCurrent failed: %x\n", eglGetError() );
        result = eglDestroyContext( mDisplay, mCtx );
        result = eglTerminate( mDisplay );
        return false;
    }
    printf( "eglMakeCurrent\n" );

    return true;
}

void Renderer::Shutdown()
{
    EGLBoolean result = eglDestroyContext( mDisplay, mCtx );
    result = eglTerminate( mDisplay );
}

