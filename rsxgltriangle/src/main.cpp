/*
 * rsxgltest - host code
 *
 * I promise this won't become a rewrite of GLUT. In fact, I plan to switch to SDL soon.
 */

#include <EGL/egl.h>
#define GL3_PROTOTYPES
#include <GL3/gl3.h>
#include <GL3/rsxgl.h>
#include <GL3/rsxgl3ext.h>

#include <net/net.h>
#include <sysutil/sysutil.h>
#include <io/pad.h>

#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#ifndef M_PI
#define M_PI 3.1415926535897932384626433
#endif

#include <time.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdarg.h>

#include "rsxgl_config.h"
#include <rsx/commands.h>

#include "math3d.h"
#include "Shader.h"
#include "Renderer.h"
#include "VertexArray.h"

static const char vertex_shader[] =
"attribute vec3 position;\n"
"\n"
"uniform mat4 ModelViewProjectionMatrix;\n"
"\n"
"\n"
"void main(void)\n"
"{\n"
"    // Transform the position to clip coordinates\n"
"    gl_Position = ModelViewProjectionMatrix * vec4(position, 1.0);\n"
"}";

static const char fragment_shader[] =
"//precision mediump float;\n"
"varying vec4 Color;\n"
"\n"
"void main(void)\n"
"{\n"
"    gl_FragColor = vec4(1.0f,0.0f,0.0f,1.0f);\n"
"}";

// quit;
int running = 1, drawing = 1;

static void
eventHandle(u64 status, u64 param, void * userdata) {
  (void)param;
  (void)userdata;
  if(status == SYSUTIL_EXIT_GAME){
    //printf("Quit app requested\n");
    //exit(0);
    running = 0;
  }
  else if(status == SYSUTIL_MENU_OPEN) {
    drawing = 0;
  }
  else if(status == SYSUTIL_MENU_CLOSE) {
    drawing = 1;
  }
  else {
    //printf("Unhandled event: %08llX\n", (unsigned long long int)status);
  }
}

void
appCleanup()
{
  sysUtilUnregisterCallback(SYSUTIL_EVENT_SLOT0);
  netDeinitialize();
  printf("Exiting for real.\n");
}

/* Convenience macros for operations on timevals.
   NOTE: `timercmp' does not work for >= or <=.  */
#define    timerisset(tvp)        ((tvp)->tv_sec || (tvp)->tv_usec)
#define    timerclear(tvp)        ((tvp)->tv_sec = (tvp)->tv_usec = 0)
#define    timercmp(a, b, CMP)                               \
  (((a)->tv_sec == (b)->tv_sec) ?                           \
   ((a)->tv_usec CMP (b)->tv_usec) :                           \
   ((a)->tv_sec CMP (b)->tv_sec))
#define    timeradd(a, b, result)                              \
  do {                                          \
    (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;                  \
    (result)->tv_usec = (a)->tv_usec + (b)->tv_usec;                  \
    if ((result)->tv_usec >= 1000000)                          \
      {                                          \
    ++(result)->tv_sec;                              \
    (result)->tv_usec -= 1000000;                          \
      }                                          \
  } while (0)
#define    timersub(a, b, result)                              \
  do {                                          \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;                  \
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;                  \
    if ((result)->tv_usec < 0) {                          \
      --(result)->tv_sec;                              \
      (result)->tv_usec += 1000000;                          \
    }                                          \
  } while (0)



void InitTriangleArray( VertexArray*& va )
{
    const float verts[] = {
        -0.5f, -0.5f, 0.0f, // bottom left
        0.5f, -0.5f, 0.0f, // bottom right
        0.0f, 0.5f, 0.0f // middle top
    };
    const unsigned int indices[] = {
        0, 1, 2
    };
    va = new VertexArray( verts, 3, indices, 3 );
    if ( !va ) {
        printf("Failed to init triangle vertex array\n");
    }
    return;
}

void DrawTriangle( Shader& shader, VertexArray* va )
{
    static GLfloat mvpMatrix[16];
    static float rotation = 0.0f;

    glClearColor( 0.0, 0.0, 0.0, 0.0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    rotation += 0.01f;
    if ( rotation > 2.0f * M_PI ) {
        rotation -= 2.0f * M_PI;
    }

    // reset mvpMatrix to identity
    identity( mvpMatrix );

    // rotate around the z axis
    rotate( mvpMatrix, rotation, 0.0f, 0.0f, 1.0f );
    
    shader.SetActive();
    shader.SetMatrixUniform( "ModelViewProjectionMatrix", mvpMatrix );

    va->Draw();
}

int
main(int argc, const char ** argv)
{
    // Test program might want to use these:
    int rsxgltest_width = 0, rsxgltest_height = 0;
    float rsxgltest_elapsed_time = 0, rsxgltest_last_time = 0, rsxgltest_delta_time = 0;
    
    Shader shader;
    Renderer renderer;
    VertexArray* triangleArray = nullptr;

    netInitialize();

    ioPadInit(1);
    padInfo padinfo;
    padData paddata;

    if ( !renderer.Init() )
    {
        return 1;
    }

    if ( !shader.Load( std::string(vertex_shader),
                       std::string(fragment_shader) ))
    {
        return 1;
    }
    shader.SetActive();
    InitTriangleArray( triangleArray );

    atexit( appCleanup );
    sysUtilRegisterCallback( SYSUTIL_EVENT_SLOT0, eventHandle, NULL );

    struct timeval start_time, current_time;
    struct timeval timeout_time = {
        .tv_sec = 6,
        .tv_usec = 0
    };

    gettimeofday(&start_time,0);
    rsxgltest_last_time = 0.0f;

    // convert to a timeval structure:
    const float ft = 1.0f / 60.0f;
    float ft_integral, ft_fractional;
    ft_fractional = modff(ft,&ft_integral);
    struct timeval frame_time = { 0,0 };
    frame_time.tv_sec = (int)ft_integral;
    frame_time.tv_usec = (int)(ft_fractional * 1.0e6);

    while ( running )
    {
        gettimeofday(&current_time,0);

        struct timeval elapsed_time;
        timersub(&current_time,&start_time,&elapsed_time);
        rsxgltest_elapsed_time = ((float)(elapsed_time.tv_sec)) + ((float)(elapsed_time.tv_usec) / 1.0e6f);
        rsxgltest_delta_time = rsxgltest_elapsed_time - rsxgltest_last_time;

        rsxgltest_last_time = rsxgltest_elapsed_time;

        ioPadGetInfo(&padinfo);
        for(size_t i = 0;i < MAX_PADS;++i) {
            if(padinfo.status[i]) {
                 ioPadGetData(i,&paddata);
                //rsxgltest_pad(i,&paddata);
                if ( paddata.BTN_START ) {
                    running = false;
                }
                break;
            }
        }

        if(drawing) {
            //EGLBoolean result = rsxgltest_draw();
            DrawTriangle( shader, triangleArray );
            EGLBoolean result = true;
            if(!result) break;
        }

        EGLBoolean result = renderer.Update();

        EGLint e = eglGetError();
        if ( !result ) {
            printf("Swap sync timed-out: %x\n",e);
            break;
        }
        else {
            struct timeval t, elapsed_time;
            gettimeofday(&t,0);
            timersub(&t,&current_time,&elapsed_time);

            if(timercmp(&elapsed_time,&frame_time,<)) {
                struct timeval sleep_time;
                timersub(&frame_time,&elapsed_time,&sleep_time);
                usleep((sleep_time.tv_sec * 1e6) + sleep_time.tv_usec);
            }

            sysUtilCheckCallback();
        }
    }

    printf("renderer shutdown\n");
    renderer.Shutdown();
    if ( triangleArray ) {
        printf("delete trianglearray\n");
        delete triangleArray;
        triangleArray = nullptr;
    }

    return 0;
}

