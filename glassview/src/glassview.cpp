/*
 * glassview - render models loaded with the assimp asset importer.
 */

#include <string>
#include <cstring>
#include <cassert>
#include <vector>

#define GL3_PROTOTYPES
#include <GL3/gl3.h>

#include "rsxgltest.h"
#include "math3d.h"
#include "sine_wave.h"
#include "VertexArray.h"

#include <stddef.h>

const char diffuse_vert_str[] =
"attribute vec3 vertex;\n"
"attribute vec3 normal;\n"
"attribute vec2 uv;\n"
"\n"
"uniform mat4 ProjMatrix;\n"
"uniform mat4 TransMatrix;\n"
"uniform mat4 NormalMatrix;\n"
"\n"
"uniform vec4 light;\n"
"\n"
"varying vec4 color;\n"
"\n"
"void\n"
"main(void)\n"
"{\n"
"  vec3 n = normalize(vec3(NormalMatrix * vec4(normal,1.0)));\n"
"  vec3 l = normalize(light.xyz);\n"
"  float diffuse = max(dot(n,l),0.0);\n"
"  color = vec4(diffuse,diffuse,diffuse,1.0);\n"
"\n"
"  vec4 v = TransMatrix * vec4(vertex.x,vertex.y,vertex.z,1);\n"
"  gl_Position = ProjMatrix * v;\n"
"}\n";

const char diffuse_frag_str[] =
"varying vec4 color;\n"
"\n"
"void\n"
"main(void)\n"
"{\n"
"  gl_FragColor = vec4(color.rgb,1.0);\n"
"}\n";


#include <io/pad.h>

#include <math.h>
#include <Eigen/Geometry>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "glassimp.h"

#include "teapot_obj.h"
#include "teddy_obj.h"
#include "crab_obj.h"
#include "yamato_obj.h"

const char * rsxgltest_name = "glassview";


struct sine_wave_t rgb_waves[3] = {
    { 0.5f,
        0.5f,
        1.0f
    },
    {
        0.5f,
        0.5f,
        1.5f
    },
    {
        0.5f,
        0.5f,
        2.5f
    }
};

GLuint shaders[2] = { 0,0 };

GLuint program = 0;

GLint ProjMatrix_location = -1, TransMatrix_location = -1, NormalMatrix_location = -1,
      vertex_location = -1, normal_location = -1, uv_location = -1,
      light_location = -1;

const GLfloat model_pitch_rate = 10.0, model_yaw_rate = 10.0, model_scale_rate = 0.05, model_scale_min = 0.1,model_scale_max = 5.0;
GLfloat model_pitch = 0, model_yaw = 0.0, model_scale = 1.0;

const GLfloat light[4] = { 10.0f, 10.0f, 10.0f, 1.0f };

// degrees per second:
#define DTOR(X) ((X)*0.01745329f)
#define RTOD(d) ((d)*57.295788f)

Eigen::Projective3f ProjMatrix(perspective(DTOR(54.3 / 2.0),1920.0 / 1080.0,0.1,1000.0));

Transform3f ViewTransform;

struct asset_model_spec {
    uint8_t const * data;
    const size_t size;

    float scale;
};

struct asset_model {
    GLuint ntris;
    GLuint nindices;
    float scale;
    VertexArray* va;

    asset_model() : 
        ntris(0), scale(1.0f), va(nullptr)
    {
    }

    ~asset_model()
    {
        if (va != nullptr) {
            delete va;
        }
    }
};

const size_t nmodels = 4;
size_t imodel = 0;

asset_model_spec model_specs[nmodels] = {
    { teapot_obj, teapot_obj_size, 1.0 },
    { teddy_obj, teddy_obj_size, 1.0 },
    { crab_obj, crab_obj_size, 1.0 },
    { yamato_obj, yamato_obj_size, 1.0 }
};

asset_model models[nmodels];

void
asset_to_gl(asset_model_spec const & model_spec, asset_model& result)
{
    Assimp::Importer importer;
    const aiScene * scene = importer.ReadFileFromMemory((char const *)model_spec.data,model_spec.size,aiProcess_PreTransformVertices | aiProcess_Triangulate,"obj");

    if(scene != 0 && scene -> mNumMeshes > 0) {
        aiMesh const * mesh = scene -> mMeshes[0];
        if(mesh != 0 && mesh -> mNumFaces > 0 && mesh -> mNumVertices > 0) {

            const GLuint nTriangles = glassimpTrianglesCount(mesh);
            printf("nTriangles: %u\n", nTriangles);
            std::vector<float> resultData;
            for (size_t i=0; i < mesh->mNumVertices; ++i)
            {
                resultData.push_back(mesh->mVertices[i].x);
                resultData.push_back(mesh->mVertices[i].y);
                resultData.push_back(mesh->mVertices[i].z);
                //printf("x, y, z: %f, %f, %f\n",
                //        mesh->mVertices[i].x,
                //        mesh->mVertices[i].y,
                //        mesh->mVertices[i].z);
                assert(mesh->mNormals != nullptr);
                resultData.push_back(mesh->mNormals[i].x);
                resultData.push_back(mesh->mNormals[i].y);
                resultData.push_back(mesh->mNormals[i].z);
                if (mesh->mTextureCoords != nullptr &&
                    mesh->mTextureCoords[0] != nullptr)
                {
                    resultData.push_back(mesh->mTextureCoords[0][i].x);
                    resultData.push_back(mesh->mTextureCoords[0][i].y);
                }
                else
                {
                    resultData.push_back(0.0f);
                    resultData.push_back(0.0f);
                }
            }

            std::vector<unsigned int> resultIndices;
            assert(mesh->mNumFaces > 0);
            for (size_t i=0; i< mesh->mNumFaces; ++i)
            {
                aiFace& face = mesh->mFaces[i];
                for (size_t j=0; j < face.mNumIndices; ++j)
                {
                    resultIndices.push_back(face.mIndices[j]);
                }
            }
            printf("nindices: %lu\n", resultIndices.size());
            result.nindices = resultIndices.size();

            result.ntris = nTriangles;

            result.va = new VertexArray(resultData.data(), resultData.size()/8,
                                        resultIndices.data(), resultIndices.size(),
                                        vertex_location, normal_location, uv_location);

            return;
        }
        else
        {
            printf("mesh null or mesh nfaces == 0 or mesh nvertices == 0\n");
        }
    }
    else
    {
        printf("scene == null or num meshes == 0\n");
    }

    printf("Failed to read scene");
}

extern "C"
void
rsxgltest_pad(unsigned int,const padData * paddata)
{
    static unsigned int square = ~0, circle = ~0;

    if(paddata -> BTN_SQUARE != square) {
        square = paddata -> BTN_SQUARE;

        if(square) {
            imodel = (imodel - 1) % nmodels;
        }
    }
    else if(paddata -> BTN_CIRCLE != circle) {
        circle = paddata -> BTN_CIRCLE;

        if(circle) {
            imodel = (imodel + 1) % nmodels;
        }
    }

    // abs of values below this get ignored:
    const float threshold = 0.05;

    const float
        left_stick_h = ((float)paddata -> ANA_L_H - 127.0f) / 127.0f,
                     left_stick_v = ((float)paddata -> ANA_L_V - 127.0f) / 127.0f,
                     right_stick_h = ((float)paddata -> ANA_R_H - 127.0f) / 127.0f,
                     right_stick_v = ((float)paddata -> ANA_R_V - 127.0f) / 127.0f;

    if(fabs(left_stick_h) > threshold) {
        model_yaw += left_stick_h * model_yaw_rate;
    }

    if(fabs(left_stick_v) > threshold) {
        model_pitch += left_stick_v * model_pitch_rate;
    }

    if(fabs(right_stick_v) > threshold) {
        model_scale += -right_stick_v * model_scale_rate;
        if(model_scale < model_scale_min) {
            model_scale = model_scale_min;
        }
        else if(model_scale > model_scale_max) {
            model_scale = model_scale_max;
        }
    }
}

extern "C"
void
rsxgltest_init(int argc,const char ** argv)
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // Set up us the program:
    shaders[0] = glCreateShader(GL_VERTEX_SHADER);
    shaders[1] = glCreateShader(GL_FRAGMENT_SHADER);

    const char* p = diffuse_vert_str;
    glShaderSource(shaders[0], 1, &p, NULL);
    glCompileShader(shaders[0]);
    GLint status;
    glGetShaderiv(shaders[0], GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        printf("Failed to compile vertex shader\n");
        exit(EXIT_FAILURE);
    }

    p = diffuse_frag_str;
    glShaderSource(shaders[1], 1, &p, NULL);
    glCompileShader(shaders[1]);
    glGetShaderiv(shaders[1], GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        printf("Failed to compile fragment shader\n");
        exit(EXIT_FAILURE);
    }

    program = glCreateProgram();

    glAttachShader(program,shaders[0]);
    glAttachShader(program,shaders[1]);
    
    // This part seems to be necessary; getting the attrib location seemed to
    // cause the program to not work...
    glBindAttribLocation(program, 0, "vertex");
    glBindAttribLocation(program, 1, "normal");
    glBindAttribLocation(program, 2, "uv");
    vertex_location = 0;
    normal_location = 1;
    uv_location = 2;

    // Link the program for real:
    glLinkProgram(program);
    glValidateProgram(program);

    summarize_program("glassview",program);

    ProjMatrix_location = glGetUniformLocation(program,"ProjMatrix");
    TransMatrix_location = glGetUniformLocation(program,"TransMatrix");
    NormalMatrix_location = glGetUniformLocation(program,"NormalMatrix");

    light_location = glGetUniformLocation(program,"light");

    glUseProgram(program);

    glUniformMatrix4fv(ProjMatrix_location,1,GL_FALSE,ProjMatrix.data());
    glUniform4fv(light_location,1,light);

    // Load the models:
    for(size_t i = 0;i < nmodels;++i) {
        asset_to_gl(model_specs[i],models[i]);
    }

#if 0
    // Draw wireframe models for now:
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glLineWidth(1.0);
#endif

    // The hell with that, draw a diffuse-shaded object, dammit:
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

    // Viewing matrix:
    ViewTransform = 
        Transform3f::Identity() *
        Eigen::Translation3f(7.806,7.419,11.525) *
        (
         Eigen::AngleAxisf(DTOR(0),Eigen::Vector3f::UnitZ()) *
         Eigen::AngleAxisf(DTOR(31.8),Eigen::Vector3f::UnitY()) *
         Eigen::AngleAxisf(DTOR(-22.538),Eigen::Vector3f::UnitX())
        );

}

// Courtesy of Matt Hall:
static
Eigen::Quaternionf quaternion_from_euler(const float pitch,const float yaw,const float roll)
{
    float ys=sinf(yaw/2.0f), yc=cosf(yaw/2.0f);
    float ps=sinf(pitch/2.0f), pc=cosf(pitch/2.0f);
    float rs=sinf(roll/2.0f), rc=cosf(roll/2.0f);

    return Eigen::Quaternionf(rs*ps*ys+rc*pc*yc,
            rc*ps*yc+rs*pc*ys,
            rc*pc*ys-rs*ps*yc,
            rs*pc*yc-rc*ps*ys);
}

extern "C"
int
rsxgltest_draw()
{
    Transform3f ViewTransformInv = ViewTransform.inverse();

    float rgb[3] = {
        compute_sine_wave(rgb_waves,rsxgltest_elapsed_time),
        compute_sine_wave(rgb_waves + 1,rsxgltest_elapsed_time),
        compute_sine_wave(rgb_waves + 2,rsxgltest_elapsed_time)
    };

    glClearColor(rgb[0] * 0.1,rgb[1] * 0.1,rgb[2] * 0.1,1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(models[imodel].va != nullptr) {

        /*
           ( Eigen::AngleAxisf(DTOR(model_yaw),Eigen::Vector3f::UnitY()) *
           Eigen::AngleAxisf(DTOR(model_pitch),Eigen::Vector3f::UnitX()) )
         */

        Transform3f transform =
            Transform3f::Identity() *
            quaternion_from_euler(DTOR(model_pitch),DTOR(model_yaw),0.0f) *
            Eigen::UniformScaling< float >(models[imodel].scale * model_scale);

        Transform3f modelview = ViewTransformInv * transform;

        Eigen::Affine3f normal = Eigen::Affine3f::Identity();
        normal.linear() = modelview.linear().inverse().transpose();

        glUniformMatrix4fv(TransMatrix_location,1,GL_FALSE,modelview.data());
        glUniformMatrix4fv(NormalMatrix_location,1,GL_FALSE,normal.data());

        models[imodel].va->Draw();


        glFlush();
    }

    return 1;
}

extern "C"
    void
rsxgltest_exit()
{
    glDeleteShader(shaders[0]);
    glDeleteShader(shaders[1]);
    glDeleteProgram(program);
}
