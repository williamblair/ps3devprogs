#include "Torus.h"

Torus::Torus()
{
}

Torus::~Torus()
{
}

bool Torus::Init( gcmContextData* context )
{
    if ( !Entity::Init( context ) ) {
        return false;
    }

    // TODO - configure
    float outerRadius = 3.0f;
    float innerRadius = 1.5f;
    u32 polyCntX = 32;
    u32 polyCntY = 32;
    return createTorus( outerRadius, innerRadius, polyCntX, polyCntY );
}

bool Torus::createTorus( f32 outerRadius, f32 innerRadius, u32 polyCntX, u32 polyCntY )
{
    u32 i,x,y,level;

    if(polyCntX<2) polyCntX = 2;
    if(polyCntY<2) polyCntY = 2;
    while(polyCntX*polyCntY>32767) {
        polyCntX /= 2;
        polyCntY /= 2;
    }

    f32 ay = 0;
    const f32 angleX = 2*M_PI/polyCntX;
    const f32 angleY = 2*M_PI/polyCntY;
    const u32 polyCntXpitch = polyCntX +1;
    const u32 polyCntYpitch = polyCntY + 1;

    m_meshBuffer->cnt_vertices = polyCntYpitch*polyCntXpitch;
    m_meshBuffer->vertices = (S3DVertex*)rsxMemalign(128,m_meshBuffer->cnt_vertices*sizeof(S3DVertex));

    m_meshBuffer->cnt_indices = polyCntY*polyCntX*6;
    m_meshBuffer->indices = (u16*)rsxMemalign(128,m_meshBuffer->cnt_indices*sizeof(u16));

    i = 0;
    for(y=0;y<=polyCntY;y++) {
        f32 axz = 0;

        const f32 sinay = sinf(ay);
        const f32 cosay = cosf(ay);
        const f32 tu = (f32)y/(f32)polyCntY;
        for(x=0;x<=polyCntX;x++) {
            const Vector3 pos(static_cast<f32>((outerRadius - (innerRadius*cosf(axz)))*cosay),
                                      static_cast<f32>((outerRadius - (innerRadius*cosf(axz)))*sinay),
                                      static_cast<f32>(innerRadius*sinf(axz)));
            
            const Vector3 nrm(static_cast<f32>(-cosf(axz)*cosay),
                                      static_cast<f32>(-cosf(axz)*sinay),
                                      static_cast<f32>(sinf(axz)));

            m_meshBuffer->vertices[i] = S3DVertex(pos.getX(),pos.getY(),pos.getZ(),nrm.getX(),nrm.getY(),nrm.getZ(),tu,(f32)x/(f32)polyCntX);

            axz += angleX;
            i++;
        }
        ay += angleY;
    }

    i = 0;
    level = 0;
    for(y=0;y<polyCntY;y++) {
        for(x=0;x<polyCntX - 1;x++) {
            const u32 curr = level + x;
            m_meshBuffer->indices[i++] = curr;
            m_meshBuffer->indices[i++] = curr + polyCntXpitch;
            m_meshBuffer->indices[i++] = curr + 1 + polyCntXpitch;
            
            m_meshBuffer->indices[i++] = curr;
            m_meshBuffer->indices[i++] = curr + 1 + polyCntXpitch;
            m_meshBuffer->indices[i++] = curr + 1;
        }

        m_meshBuffer->indices[i++] = level + polyCntX;
        m_meshBuffer->indices[i++] = level + polyCntX - 1;
        m_meshBuffer->indices[i++] = level + polyCntX - 1 + polyCntXpitch;
        
        m_meshBuffer->indices[i++] = level + polyCntX;
        m_meshBuffer->indices[i++] = level + polyCntX - 1 + polyCntXpitch;
        m_meshBuffer->indices[i++] = level + polyCntX + polyCntXpitch;

        level += polyCntXpitch;
    }

    return true;
}


