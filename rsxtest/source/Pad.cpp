#include "Pad.h"

bool Pad::m_sysPadInitted = false;

Pad::Pad()
{
    m_padnum = 0;
    m_isActive = false;
    memset( (void*)&m_paddata, 0, sizeof(m_paddata) );
    memset( (void*)&m_prevPadData, 0, sizeof(m_prevPadData) );
    memset( (void*)&m_actparam, 0, sizeof(m_actparam) );
}

Pad::~Pad()
{
}

void Pad::Update()
{
    ioPadGetInfo( &m_padinfo );
    if ( m_padinfo.status[ m_padnum ] )
    {
        m_isActive = true;
        memcpy( (void*)&m_prevPadData, (void*)&m_paddata, sizeof(m_prevPadData) );
        ioPadGetData( m_padnum, &m_paddata );
    }
    else
    {
        m_isActive = false;
    }
}

bool Pad::IsHeld( Button b )
{
    if ( !m_isActive ) {
        return false;
    }
    bool held = false;
    switch (b)
    {
    case LEFT:     held = (bool)m_paddata.BTN_LEFT;     break;
    case DOWN:     held = (bool)m_paddata.BTN_DOWN;     break;
    case RIGHT:    held = (bool)m_paddata.BTN_RIGHT;    break;
    case UP:       held = (bool)m_paddata.BTN_UP;       break;
    case SQUARE:   held = (bool)m_paddata.BTN_SQUARE;   break;
    case CROSS:    held = (bool)m_paddata.BTN_CROSS;    break;
    case CIRCLE:   held = (bool)m_paddata.BTN_CIRCLE;   break;
    case TRIANGLE: held = (bool)m_paddata.BTN_TRIANGLE; break;
    default:
        break;
    }

    return held;
}

bool Pad::IsClicked( Button b )
{
    if ( !m_isActive ) {
        return false;
    }
    bool clicked = false;
    switch (b)
    {
    case LEFT:     clicked = (!m_paddata.BTN_LEFT && m_prevPadData.BTN_LEFT);          break;
    case DOWN:     clicked = (!m_paddata.BTN_DOWN && m_prevPadData.BTN_DOWN);          break;
    case RIGHT:    clicked = (!m_paddata.BTN_RIGHT && m_prevPadData.BTN_RIGHT);        break;
    case UP:       clicked = (!m_paddata.BTN_UP && m_prevPadData.BTN_UP);              break;
    case SQUARE:   clicked = (!m_paddata.BTN_SQUARE && m_prevPadData.BTN_SQUARE);      break;
    case CROSS:    clicked = (!m_paddata.BTN_CROSS && m_prevPadData.BTN_CROSS);        break;
    case CIRCLE:   clicked = (!m_paddata.BTN_CIRCLE && m_prevPadData.BTN_CIRCLE);      break;
    case TRIANGLE: clicked = (!m_paddata.BTN_TRIANGLE && m_prevPadData.BTN_TRIANGLE);  break;
    default:
        break;
    }

    return clicked;
}

bool Pad::SetSmallActuator( bool isOn )
{
    if ( m_isActive )
    {
        m_actparam.small_motor = (int)isOn;
        ioPadSetActDirect( m_padnum, &m_actparam );
        return true;
    }
    return false;
}

bool Pad::SetLargeActuator( unsigned char val )
{
    if ( m_isActive )
    {
        m_actparam.large_motor = val;
        ioPadSetActDirect( m_padnum, &m_actparam );
        return true;
    }
    return false;
}

