#include "Sphere.h"

Sphere::Sphere()
{
}

Sphere::~Sphere()
{
}


bool Sphere::Init( gcmContextData* context )
{
    if ( !Entity::Init( context ) ) {
        return false;
    }

    // TODO - configure
    float radius = 3.0f;
    u32 polyCntX = 32;
    u32 polyCntY = 32;
    return createSphere( radius, polyCntX, polyCntY );
}

bool Sphere::createSphere( f32 radius, u32 polyCntX, u32 polyCntY )
{
    u32 i,p1,p2,level;
    u32 x,y,polyCntXpitch;
    const f32 RECIPROCAL_PI = 1.0f/M_PI;

    if(polyCntX<2) polyCntX = 2;
    if(polyCntY<2) polyCntY = 2;
    if(polyCntX*polyCntY>32767) {
        if(polyCntX>polyCntY) 
            polyCntX = 32767/polyCntY-1;
        else
            polyCntY = 32767/(polyCntX+1);
    }
    polyCntXpitch = polyCntX+1;

    m_meshBuffer->cnt_vertices = (polyCntXpitch*polyCntY)+2;
    m_meshBuffer->vertices = (S3DVertex*)rsxMemalign(128,m_meshBuffer->cnt_vertices*sizeof(S3DVertex));

    m_meshBuffer->cnt_indices = (polyCntX*polyCntY)*6;
    m_meshBuffer->indices = (u16*)rsxMemalign(128,m_meshBuffer->cnt_indices*sizeof(u16));

    i = 0;
    level = 0;
    for(p1=0;p1<polyCntY-1;p1++) {
        for(p2=0;p2<polyCntX-1;p2++) {
            const u32 curr = level + p2;
            m_meshBuffer->indices[i++] = curr;
            m_meshBuffer->indices[i++] = curr + polyCntXpitch;
            m_meshBuffer->indices[i++] = curr + 1 + polyCntXpitch;

            m_meshBuffer->indices[i++] = curr;
            m_meshBuffer->indices[i++] = curr + 1 + polyCntXpitch;
            m_meshBuffer->indices[i++] = curr + 1;
        }

        m_meshBuffer->indices[i++] = level + polyCntX;
        m_meshBuffer->indices[i++] = level + polyCntX - 1;
        m_meshBuffer->indices[i++] = level + polyCntX - 1 + polyCntXpitch;

        m_meshBuffer->indices[i++] = level + polyCntX;
        m_meshBuffer->indices[i++] = level + polyCntX - 1 + polyCntXpitch;
        m_meshBuffer->indices[i++] = level + polyCntX + polyCntXpitch;

        level += polyCntXpitch;
    }

    const u32 polyCntSq = polyCntXpitch*polyCntY;
    const u32 polyCntSq1 = polyCntSq+1;
    const u32 polyCntSqM1 = (polyCntY-1)*polyCntXpitch;

    for(p2=0;p2<polyCntX-1;p2++) {
        m_meshBuffer->indices[i++] = polyCntSq;
        m_meshBuffer->indices[i++] = p2;
        m_meshBuffer->indices[i++] = p2+1;

        m_meshBuffer->indices[i++] = polyCntSq1;
        m_meshBuffer->indices[i++] = polyCntSqM1+p2;
        m_meshBuffer->indices[i++] = polyCntSqM1+p2+1;
    }

    m_meshBuffer->indices[i++] = polyCntSq;
    m_meshBuffer->indices[i++] = polyCntX-1;
    m_meshBuffer->indices[i++] = polyCntX;

    m_meshBuffer->indices[i++] = polyCntSq1;
    m_meshBuffer->indices[i++] = polyCntSqM1;
    m_meshBuffer->indices[i++] = polyCntSqM1+polyCntX-1;

    f32 axz;
    f32 ay = 0;
    const f32 angelX = 2*M_PI/polyCntX;
    const f32 angelY = M_PI/polyCntY;

    i = 0;
    for(y=0;y<polyCntY;y++) {
        axz = 0;
        ay += angelY;
        const f32 sinay = sinf(ay);
        for(x=0;x<polyCntX;x++) {
            const Vector3 pos(static_cast<f32>(radius*cosf(axz)*sinay), static_cast<f32>(radius*cosf(ay)), static_cast<f32>(radius*sinf(axz)*sinay));
            
            Vector3 normal = normalize(pos);
            
            f32 tu = 0.5F;
            if(y==0) {
                if(normal.getY()!=-1.0F && normal.getY()!=1.0F)
                    tu = static_cast<f32>(acosf(clamp(normal.getX()/sinay,-1.0f,1.0f))*0.5F*RECIPROCAL_PI);
                if(normal.getZ()<0.0F)
                    tu = 1-tu;
            } else
                tu = m_meshBuffer->vertices[i - polyCntXpitch].u;

            m_meshBuffer->vertices[i] = S3DVertex(pos.getX(),pos.getY(),pos.getZ(),normal.getX(),normal.getY(),normal.getZ(),tu,static_cast<f32>(ay*RECIPROCAL_PI));
            axz += angelX;
            i++;
        }
        m_meshBuffer->vertices[i] = S3DVertex(m_meshBuffer->vertices[i-polyCntX]);
        m_meshBuffer->vertices[i].u = 1.0F;
        i++;
    }

    m_meshBuffer->vertices[i++] = S3DVertex(0.0F,radius,0.0F,0.0F,1.0F,0.0F,0.5F,0.0F);
    m_meshBuffer->vertices[i] = S3DVertex(0.0F,-radius,0.0F,0.0F,-1.0F,0.0F,0.5F,1.0F);

    return true;
}

