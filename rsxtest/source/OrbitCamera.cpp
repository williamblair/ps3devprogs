#include "OrbitCamera.h"

OrbitCamera::OrbitCamera() :
    m_offset(0.0f, 20.0f/**20.0f*/, -20.0f/**20.0f*/),
    m_up(0.0f, 1.0f, 0.0f),
    m_target(0.0f, 3.0f/**20.0f*/, 0.0f),
    m_pitchSpeed(0.0f),
    m_yaw(0.0f),
    m_yawSpeed(0.0f)
{
}

OrbitCamera::~OrbitCamera()
{
}

void OrbitCamera::Update( const float dt )
{
    // track total internal yaw
    m_yaw += m_yawSpeed * dt;

    // yaw quaternion about world up
    Vectormath::Aos::Quat yaw = Vectormath::Aos::Quat::rotationY(
        m_yawSpeed * dt );
 
    // transform offset and up by yaw
    m_offset = Vectormath::Aos::rotate( yaw, m_offset );
    m_up = Vectormath::Aos::rotate( yaw, m_up );
    
    // Compute camera forward and right
    // forward owner.position - (owner.position + offset)
    // = -offset
    Vectormath::Aos::Vector3 forward = -1.0f * m_offset;
    forward = normalize( forward );
    Vectormath::Aos::Vector3 right = Vectormath::Aos::cross( m_up, forward );
    right = Vectormath::Aos::normalize( right );
    
    // Create quaternion for pitch about camera right
    Vectormath::Aos::Quat pitch = Vectormath::Aos::Quat::rotation(
        m_pitchSpeed * dt, right );
    // transform offset and up by pitch
    m_offset = Vectormath::Aos::rotate( pitch, m_offset );
    m_up = Vectormath::Aos::rotate( pitch, m_up );
    
    // Create the lookat matrix
    m_viewMat = Vectormath::Aos::Matrix4::lookAt(
        Vectormath::Aos::Point3( m_target + m_offset ), // camera position
        Vectormath::Aos::Point3( m_target ), // camera target/look at
        m_up ); // up vector
}

