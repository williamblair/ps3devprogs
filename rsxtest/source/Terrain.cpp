#include <Terrain.h>
#include <Log.h>

Terrain::Terrain() :
    m_width( 0 ),
    m_minX( 0.0f ),
    m_minZ( 0.0f ),
    m_maxX( 0.0f ),
    m_maxZ( 0.0f )
{
}

Terrain::~Terrain()
{
}

bool Terrain::Init( gcmContextData* context,
                    const std::string& rawFileName,
                    const unsigned int width,
                    const float heightScale,
                    const float sizeScale )
{
    m_width = width;
    
    std::ifstream rawFile( rawFileName, std::ios::binary );
    if ( !rawFile.is_open() ) {
        //std::cerr << __func__ << ": failed to open " << rawFileName << std::endl;
        DBG_LOG( "Failed to open %s\n", rawFileName.c_str() );
        return false;
    }
    std::vector<uint8_t> fileBytes;
    rawFile.seekg( 0, std::ios::end );
    size_t fileSize = rawFile.tellg();
    rawFile.seekg( 0, std::ios::beg );
    fileBytes.resize( fileSize );
    rawFile.read( (char*)fileBytes.data(), fileBytes.size() );
    if ( fileBytes.size() != width * width ) {
        //std::cerr << "File size bytes != width*width" << std::endl;
        DBG_LOG( "File size bytes != width*width\n" );
        return false;
    }

    m_heights.resize( width * width );
    m_colors.resize( width * width );
    for ( size_t i = 0; i < m_heights.size(); ++i ) {
        m_heights[i] = (((float)fileBytes[i]) / 256.0f) * heightScale;
        //colors[i].x = 0.0f;
        //colors[i].y = ((float)fileBytes[i]) / 256.0f;
        //colors[i].z = 0.0f;
        // Let texture do the coloring
        m_colors[i].setX( 1.0f );
        m_colors[i].setY( 1.0f );
        m_colors[i].setZ( 1.0f );
    }

    GenerateVertices( sizeScale );
    GenerateTexCoords();
    GenerateIndices();
    GenerateNormals();

    //GenerateWaterVertices();
    //GenerateWaterTexCoords();
    //GenerateWaterIndices();
    
    if ( !Entity::Init( context ) ) {
        DBG_LOG( "Failed to init entity\n" );
        return false;
    }

    return GenerateBuffers();
}

void Terrain::GenerateVertices( const float sizeScale )
{
    size_t i = 0;
    m_positions.resize( m_width * m_width );
    for (float z = float(-((int)m_width) / 2); z <= (int)m_width / 2; z++) {
        for (float x = float(-((int)m_width) / 2); x <= (int)m_width / 2; x++) {
            if (i >= m_positions.size()) {
                //std::cerr << "INVALID I!!!!" << std::endl;
                DBG_LOG( "INVALID I!!!!\n" );
                return;
            }
            m_positions[i].setX( x );
            m_positions[i].setY( m_heights[i] );
            m_positions[i].setZ( z );

            if ( x < m_minX ) m_minX = x;
            if ( x > m_maxX ) m_maxX = x;
            if ( z < m_minZ ) m_minZ = z;
            if ( z > m_maxZ ) m_maxZ = z;

            i++;
        }
    }
}

void Terrain::GenerateTexCoords()
{
    // TODO - precaculate size
    m_texCoords.clear();
    for ( int z = 0; z < int( m_width ); ++z ) {
        for ( int x = 0; x < int( m_width ); ++x ) {
            float s = (float(x) / float(m_width)) * 8.0f;
            float t = (float(z) / float(m_width)) * 8.0f;
            m_texCoords.push_back( Vector2( s, t ) );
        }
    }
}

void Terrain::GenerateNormals()
{
    std::vector<Vectormath::Aos::Vector3> faceNormals; // tmp calculation
    std::vector<int> shareCount;

    m_normals.resize( m_positions.size() );
    shareCount.resize( m_positions.size() );

    for ( size_t i = 0; i < shareCount.size(); ++i ) {
        shareCount[i] = 0;
    }

    size_t numTriangles = m_indices.size() / 3;
    faceNormals.resize( numTriangles );
    
    for ( unsigned int i = 0; i < numTriangles; ++i ) {
        Vectormath::Aos::Vector3* v1 = &m_positions[m_indices[(i*3) + 0]];
        Vectormath::Aos::Vector3* v2 = &m_positions[m_indices[(i*3) + 1]];
        Vectormath::Aos::Vector3* v3 = &m_positions[m_indices[(i*3) + 2]];

        Vectormath::Aos::Vector3 vec1, vec2;

        //vec1.setX( v2->getX() - v1->getX() );
        //vec1.setY( v2->gety() - v1->getY() );
        //vec1.setZ( v2->getZ() - v1->getZ() );
        vec1 = *v2 - *v1;

        //vec2.setX( v3->getX() - v1->getX() );
        //vec2.setY( v3->getY() - v1->getY() );
        //vec2.setZ( v3->getZ() - v1->getZ() );
        vec2 = *v3 - *v1;

        Vectormath::Aos::Vector3* normal = &faceNormals[i];
        *normal = Vectormath::Aos::cross( vec1, vec2 );
        *normal = Vectormath::Aos::normalize( *normal );

        for ( int j = 0; j < 3; ++j ) {
            int index = m_indices[(i*3) + j];
            //m_normals[index].x += normal->getX();
            //m_normals[index].y += normal->getY();
            //m_normals[index].z += normal->getZ();
            m_normals[index] += *normal;
            shareCount[index]++;
        }
    }

    for ( unsigned int i = 0; i < m_positions.size(); ++i ) {
        //normals[i].x = normals[i].x / shareCount[i];
        //normals[i].y = normals[i].y / shareCount[i];
        //normals[i].z = normals[i].z / shareCount[i];
        m_normals[i] /= float( shareCount[i] );
        m_normals[i] = Vectormath::Aos::normalize( m_normals[i] );
    }
}

void Terrain::GenerateIndices()
{
    /*
        We loop through building the triangles that
        make up each grid square in the heightmap

        (z*w+x) *----* (z*w+x+1)
                |   /| 
                |  / | 
                | /  |
        ((z+1)*w+x)*----* ((z+1)*w+x+1)
    */
    m_indices.clear();
    for ( int z = 0; z < int( m_width ) - 1; ++z ) {
        for ( int x = 0; x < int( m_width ) - 1; ++x ) {
            m_indices.push_back( z     * m_width + x );
            m_indices.push_back( (z+1) * m_width + x );
            m_indices.push_back( z     * m_width + x + 1 );

            m_indices.push_back( (z+1) * m_width + x );
            m_indices.push_back( (z+1) * m_width + x + 1 );
            m_indices.push_back( z     * m_width + x + 1 );
        }
    }
}

void Terrain::GenerateWaterVertices()
{
    size_t i = 0;
    m_waterPositions.resize( m_width * m_width );
    for (float z = float(-((int)m_width) / 2); z <= (int)m_width / 2; z++) {
        for (float x = float(-((int)m_width) / 2); x <= (int)m_width / 2; x++) {
            if (i >= m_waterPositions.size()) {
                //std::cerr << "INVALID I!!!!" << std::endl;
                DBG_LOG( "INVALID I!!!!\n" );
                return;
            }
            m_waterPositions[i].setX( x );
            m_waterPositions[i].setY( 4.0f );
            m_waterPositions[i].setZ( z );
            i++;
        }
    }
}

void Terrain::GenerateWaterIndices()
{
    /*
        We loop through building the triangles that
        make up each grid square in the heightmap

        (z*w+x) *----* (z*w+x+1)
                |   /| 
                |  / | 
                | /  |
        ((z+1)*w+x)*----* ((z+1)*w+x+1)
    */
    m_waterIndices.clear();
    for (int z = 0; z < int( m_width ) - 1; ++z) {
        for (int x = 0; x < int( m_width ) - 1; ++x) {
           m_waterIndices.push_back( z     * m_width + x );
           m_waterIndices.push_back( (z+1) * m_width + x );
           m_waterIndices.push_back( z     * m_width + x + 1 );

           m_waterIndices.push_back( (z+1) * m_width + x );
           m_waterIndices.push_back( (z+1) * m_width + x + 1 );
           m_waterIndices.push_back( z     * m_width + x + 1 );
        }
    }
}

void Terrain::GenerateWaterTexCoords()
{
    m_waterTexCoords.clear();
    for (int z = 0; z < int( m_width ); ++z) {
        for (int x = 0; x < int( m_width ); ++x) {
            float s = (float(x) / float(m_width)) * 8.0f;
            float t = (float(z) / float(m_width)) * 8.0f;
            m_waterTexCoords.push_back( Vector2( s,t ) );
        }
    }
}

bool Terrain::GenerateBuffers()
{
    m_meshBuffer->cnt_vertices = m_positions.size();
    m_meshBuffer->vertices =
        (S3DVertex*)rsxMemalign( 128,
                                 m_meshBuffer->cnt_vertices *
                                 sizeof( S3DVertex ));
    if ( !m_meshBuffer->vertices ) {
        DBG_LOG( "Failed to alloc vertices\n" );
        return false;
    }
    m_meshBuffer->cnt_indices = m_indices.size();
    m_meshBuffer->indices = nullptr;
    m_meshBuffer->indices32 =
        (u32*)rsxMemalign( 128,
                           m_meshBuffer->cnt_indices * sizeof(u32) );
    if ( !m_meshBuffer->indices32 ) {
        DBG_LOG( "Failed to alloc indices\n" );
        return false;
    }

    for ( u32 i = 0; i < m_meshBuffer->cnt_vertices; ++i )
    {
        Vectormath::Aos::Vector3& pos = m_positions[i];
        Vectormath::Aos::Vector3& nrm = m_normals[i];
        Vector2& texCoord = m_texCoords[i];
        m_meshBuffer->vertices[i] =
            S3DVertex( pos.getX(), pos.getY(), pos.getZ(),
                       nrm.getX(), nrm.getY(), nrm.getZ(),
                       texCoord.u, texCoord.v );
    }

    for ( u32 i = 0; i < m_meshBuffer->cnt_indices; ++i )
    {
        m_meshBuffer->indices32[i] = m_indices[i];
    }

    return true;
}

