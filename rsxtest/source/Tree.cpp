#include <Tree.h>
#include <Log.h>

// static buffer instance
S3DVertex* Tree::s_vertices = nullptr;

Tree::~Tree()
{
    if ( s_vertices != nullptr )
    {
        // TODO - replace other rsx allocations free with rsxFree()
        rsxFree( s_vertices );
        s_vertices = nullptr;
    }
}

bool Tree::Init( gcmContextData* context )
{
    if ( !Entity::Init( context ) ) {
        DBG_LOG( "Failed to init entity\n" );
        return false;
    }

    static const size_t numVertices = 12;
    m_meshBuffer->cnt_vertices = numVertices;
    if ( s_vertices == nullptr )
    {
        float vertices[numVertices*3] = 
        {
            // first square
            -1.0f, 0.0f, 0.0f, // bottom left
            1.0f,  0.0f, 0.0f, // bottom right
            -1.0f, 2.0f, 0.0f, // top left
            1.0f,  0.0f, 0.0f, // bottom right
            1.0f,  2.0f, 0.0f, // top right
            -1.0f, 2.0f, 0.0f, // top left
    
            // second square
            0.0f, 0.0f,  1.0f, // bottom left
            0.0f, 0.0f, -1.0f, // bottom right
            0.0f, 2.0f,  1.0f, // top left
            0.0f, 0.0f, -1.0f, // bottom right
            0.0f, 2.0f, -1.0f, // top right
            0.0f, 2.0f,  1.0f // top left
            
        };
    
        float texCoords[numVertices*2] =
        {
            // first square
            0.0f, 0.0f, // bottom left
            1.0f, 0.0f, // bottom right
            0.0f, 1.0f, // top left
            1.0f, 0.0f, // bottom right
            1.0f, 1.0f, // top right
            0.0f, 1.0f, // top left
    
            // second square
            0.0f, 0.0f, // bottom left
            1.0f, 0.0f, // bottom right
            0.0f, 1.0f, // top left
            1.0f, 0.0f, // bottom right
            1.0f, 1.0f, // top right
            0.0f, 1.0f, // top left
        };

        s_vertices = (S3DVertex*)rsxMemalign( 128,
                                              m_meshBuffer->cnt_vertices * sizeof(S3DVertex) );
        for ( size_t i = 0; i < numVertices; ++i )
        {
            float* vertex = &vertices[i*3];
            float* texCoord = &texCoords[i*2];
            s_vertices[i] = S3DVertex( vertex[0], vertex[1], vertex[2],
                                        0.0f, 1.0f, 0.0f, // TODO - normals
                                        texCoord[0], texCoord[1] );
        }
    }
    m_meshBuffer->vertices = s_vertices;
    m_meshBuffer->cnt_indices = 0;
    m_meshBuffer->indices = nullptr;
    m_meshBuffer->indices32 = nullptr;

    return true;
}

