#include <cassert>
#include <list>
#include "Entity.h"
#include "Log.h"

#define DBG_ASSERT(cond) assert(cond)
//#define DBG_ASSERT(cond)

Entity::Entity() :
    m_context( nullptr ),
    m_meshBuffer( nullptr ),
    m_position( 0.0f, 0.0f, 0.0f ),
    m_yawSpeed( 0.0f ),
    m_yaw( 0.0f ),
    m_yawOffset( 0.0f ),
    m_yawOffsetSpeed( 0.0f ),
    m_pitchSpeed( 0.0f ),
    m_pitch( 0.0f ),
    m_forwardSpeed( 0.0f )
{
}

Entity::~Entity()
{
    if ( m_meshBuffer != nullptr )
    {
        delete m_meshBuffer;
    }
}

bool Entity::Init( gcmContextData* context )
{
    m_context = context;
    m_meshBuffer = new SMeshBuffer();
    return ( m_meshBuffer != nullptr );
}

void Entity::Render()
{
    u32 offset = 0;

    //DBG_ASSERT( m_meshBuffer != nullptr );
    if ( m_meshBuffer == nullptr ||
         (m_meshBuffer->cnt_vertices == 0 &&
         m_meshBuffer->cnt_indices == 0 ))

    {
        //DBG_LOG( "mesh buffer null or cnt vertices/indices both 0\n" );
        return;
    }
    //DBG_LOG( "Rendering, cnt_vertices/indices: %u, %u\n", m_meshBuffer->cnt_vertices, m_meshBuffer->cnt_indices );
    rsxAddressToOffset( &m_meshBuffer->vertices[0].pos, &offset );
    rsxBindVertexArrayAttrib( m_context,
                              GCM_VERTEX_ATTRIB_POS,
                              0,
                              offset,
                              sizeof( S3DVertex ),
                              3,
                              GCM_VERTEX_DATA_TYPE_F32,
                              GCM_LOCATION_RSX );

    rsxAddressToOffset( &m_meshBuffer->vertices[0].nrm, &offset );
    rsxBindVertexArrayAttrib( m_context,
                              GCM_VERTEX_ATTRIB_NORMAL,
                              0,
                              offset,
                              sizeof( S3DVertex ),
                              3,
                              GCM_VERTEX_DATA_TYPE_F32,
                              GCM_LOCATION_RSX );

    rsxAddressToOffset( &m_meshBuffer->vertices[0].u, &offset );
    rsxBindVertexArrayAttrib( m_context,
                              GCM_VERTEX_ATTRIB_TEX0,
                              0,
                              offset,
                              sizeof( S3DVertex ),
                              2,
                              GCM_VERTEX_DATA_TYPE_F32,
                              GCM_LOCATION_RSX );

    if ( m_meshBuffer->indices != nullptr )
    {
        rsxAddressToOffset( &m_meshBuffer->indices[0], &offset );
        rsxDrawIndexArray( m_context,
                           GCM_TYPE_TRIANGLES,        // u32 type
                           offset,                    // u32 offset
                           m_meshBuffer->cnt_indices, // u32 count
                           GCM_INDEX_TYPE_16B,        // u32 data_type
                           GCM_LOCATION_RSX );        // u32 location
    }
    else if ( m_meshBuffer->indices32 != nullptr )
    {
        rsxAddressToOffset( &m_meshBuffer->indices32[0], &offset );
        rsxDrawIndexArray( m_context,
                           GCM_TYPE_TRIANGLES,        // u32 type
                           offset,                    // u32 offset
                           m_meshBuffer->cnt_indices, // u32 count
                           GCM_INDEX_TYPE_32B,        // u32 data_type
                           GCM_LOCATION_RSX );        // u32 location
    }
    else
    {
        rsxDrawVertexArray( m_context,
                            GCM_TYPE_TRIANGLES,
                            0,
                            m_meshBuffer->cnt_vertices );
    }
}

void Entity::Update( float dt )
{
    m_yaw += m_yawSpeed * dt;
    m_yawOffset += m_yawOffsetSpeed * dt;
    
    // TODO - account for pitch in forward as well
    // default forward points right; along x axis (0 degrees yaw)
    Vectormath::Aos::Vector3 forward( 1.0f, 0.0f, 0.0f );
    // rotate the right-pointing forward about Y axis based on yaw
    Vectormath::Aos::Quat yaw = Vectormath::Aos::Quat::rotationY( m_yaw + m_yawOffset );
    forward = Vectormath::Aos::rotate( yaw, forward );

    // move forward
    m_position = m_position + (forward * m_forwardSpeed * dt);
    
    // TODO
    // This doesn't allow for pitch or roll rotation...
    m_modelMat = Vectormath::Aos::Matrix4(
        Vectormath::Aos::Transform3(
            Vectormath::Aos::Quat::rotationY( m_yaw + m_yawOffset ),
            m_position ));
}

#undef DBG_ASSERT

