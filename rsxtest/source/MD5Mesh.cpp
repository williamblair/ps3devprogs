#include "MD5Mesh.h"
#include "Log.h"

MD5Mesh::MD5Mesh(const MD5FileData&t) :  type(t) {
#ifdef MD5_USE_HARDWARE_SKINNING
    weightObject = 0;
    weights         = NULL;
#endif

    numVertices = 0;
    numIndices = 0;
    vertices = nullptr;
    indices = nullptr;
    colors = nullptr;
    textureCoords = nullptr;
}

bool MD5Mesh::Init( gcmContextData* context )
{
    return Entity::Init( context );
}

MD5Mesh::~MD5Mesh(void)    {
#ifdef MD5_USE_HARDWARE_SKINNING
    delete weights;
    glDeleteBuffers(1, &weightObject);
#endif

    for ( unsigned int i = 0; i < children.size(); ++i ) {
        MD5Mesh* child = children.at(i);
        if ( child->vertices != nullptr ) free( child->vertices );
        if ( child->textureCoords != nullptr ) delete[] child->textureCoords;
        if ( child->indices != nullptr ) delete[] child->indices;
        delete children.at(i);
    }
}

///*
//Draws the current MD5Mesh. The handy thing about overloaded virtual functions
//is that they can still run the code they have 'overridden', by calling the 
//parent class function as you would a static function. So all of the new stuff
//you've been building up in the Mesh class as the tutorials go on, will 
//automatically be used by this overloaded function. Once 'this' has been drawn,
//all of the children of 'this' will be drawn
//*/

void MD5Mesh::Render() {

    //GL_BREAKPOINT;
    if(numVertices == 0) {
        
        //DBG_LOG( "Rendering root node children: %lu\n", children.size() );
        
        //Assume that this mesh is actually our 'root' node
        //so set up the shader with our TBOs
#ifdef MD5_USE_HARDWARE_SKINNING
        type.BindTextureBuffers();
#endif

        for(unsigned int i = 0; i < children.size(); ++i) {
            //children[i]->Draw();
            //DBG_LOG( "Rendering child %u\n", i );
            children[i]->Render();
        }
    }
    //Mesh::Draw();
    //DBG_LOG( "Rendering root node, numVerices, numIndices, numChildren: %u, %u, %lu\n", numVertices, numIndices, children.size() );
    Entity::Render();
};

bool MD5Mesh::BufferData()
{
#if 0
    glBindVertexArray(arrayObject);
    glGenBuffers(1, &bufferObject[VERTEX_BUFFER]);
    glBindBuffer(GL_ARRAY_BUFFER, bufferObject[VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER,
        numVertices*sizeof(Vector3),
        vertices,
        GL_STATIC_DRAW);
    glVertexAttribPointer(VERTEX_BUFFER,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        0);
    glEnableVertexAttribArray(VERTEX_BUFFER);

    if (colors)
    {
        glGenBuffers(1, &bufferObject[COLOUR_BUFFER]);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[COLOUR_BUFFER]);
        glBufferData(GL_ARRAY_BUFFER, 
            numVertices*sizeof(Vector4),
            colors,
            GL_STATIC_DRAW);
        glVertexAttribPointer(COLOUR_BUFFER,
            4,
            GL_FLOAT,
            GL_FALSE,
            0,
            0);
        glEnableVertexAttribArray(COLOUR_BUFFER);
    }

    if (textureCoords)
    {
        glGenBuffers(1, &bufferObject[TEXTURE_BUFFER]);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TEXTURE_BUFFER]);
        glBufferData(GL_ARRAY_BUFFER,
            numVertices*sizeof(Vector2),
            textureCoords,
            GL_STATIC_DRAW);
        glVertexAttribPointer(TEXTURE_BUFFER,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            0);
        glEnableVertexAttribArray(TEXTURE_BUFFER);
    }

    if (indices)
    {
        glGenBuffers(1, &bufferObject[INDEX_BUFFER]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject[INDEX_BUFFER]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
            numIndices * sizeof(GLuint), 
            indices, 
            GL_STATIC_DRAW);
    }

    glBindVertexArray(0);
#endif
    DBG_LOG( "Generating RSX buffers\n" );
    
    if ( vertices == nullptr ) {
        DBG_LOG( "vertices null!\n" );
        return false;
    }
    m_meshBuffer->cnt_vertices = numVertices;
    m_meshBuffer->vertices =
        (S3DVertex*)rsxMemalign( 128,
                                 m_meshBuffer->cnt_vertices * sizeof(S3DVertex) );
    if ( !m_meshBuffer->vertices ) {
        DBG_LOG( "Failed to alloc vertices\n" );
        return false;
    }
    
    if ( indices )
    {
        m_meshBuffer->cnt_indices = numIndices;
        m_meshBuffer->indices = nullptr;
        m_meshBuffer->indices32 =
            (u32*)rsxMemalign( 128, m_meshBuffer->cnt_indices * sizeof(u32) );
        if ( !m_meshBuffer->indices32 ) {
            DBG_LOG( "Failed to alloc indices32\n" );
            free( m_meshBuffer->vertices );
            return false;
        }
    }
    
    for ( u32 i = 0; i < m_meshBuffer->cnt_vertices; ++i )
    {
        Vectormath::Aos::Vector3& pos = vertices[i];
        Vector2& texCoord = textureCoords[i];
        m_meshBuffer->vertices[i] = S3DVertex( pos.getX(), pos.getY(), pos.getZ(),
                                               0.0f, 1.0f, 0.0f, // TODO - normals
                                               texCoord.u, 1.0f - texCoord.v );
    }
    
    DBG_LOG( "mesh buffer num indices: %u\n", m_meshBuffer->cnt_indices );
    for ( u32 i = 0; i < m_meshBuffer->cnt_indices; ++i )
    {
        m_meshBuffer->indices32[i] = indices[i];
    }
    
    return true;
}

///*
//Skins each vertex by its weightings, producing a final skinned mesh in the passed in
//skeleton pose. 
//*/
void    MD5Mesh::SkinVertices(const MD5Skeleton &skel) {

    //For each submesh, we want to transform a position for each vertex
    for(unsigned int i = 0; i < type.numSubMeshes; ++i) {
        MD5SubMesh& subMesh = type.subMeshes[i];    //Get a reference to the current submesh
        /*
        Each MD5SubMesh targets a Mesh's data. The first submesh will target 'this', 
        while subsequent meshes will target the children of 'this'
        */
        MD5Mesh*target        = (MD5Mesh*)children.at(i);

        /*
        For each vertex in the submesh, we want to build up a final position, taking
        into account the various weighting anchors used.
        */
        for(int j = 0; j < subMesh.numverts; ++j) {
            //UV coords can be copied straight over to the Mesh textureCoord array
            target->textureCoords[j]   = subMesh.verts[j].texCoords;

            //And we should start off with a Vector of 0,0,0
            target->vertices[j] = Vectormath::Aos::Vector3( 0.0f, 0.0f, 0.0f );

            /*
            Each vertex has a number of weights, determined by weightElements. The first
            of these weights will be in the submesh weights array, at position weightIndex.

            Each of these weights has a joint it is in relation to, and a weighting value,
            which determines how much influence the weight has on the final vertex position
            */

            for ( int k = 0; k < subMesh.verts[j].weightElements; ++k ) {
                MD5Weight& weight    = subMesh.weights[subMesh.verts[j].weightIndex + k];
                MD5Joint& joint        = skel.joints[weight.jointIndex];

                /*
                We can then transform the weight position by the joint's world transform, and multiply
                the result by the weightvalue. Finally, we add this value to the vertex position, eventually
                building up a weighted vertex position.
                */

                Vectormath::Aos::Vector4 res = (( joint.transform *
                                                  Vectormath::Aos::Vector4( weight.position, 1.0f ))
                                                * weight.weightValue );
                target->vertices[j] += res.getXYZ();
            }
        }

        /*
        As our vertices have moved, normals and tangents must be regenerated!
        */
#ifdef MD5_USE_NORMALS
        target->GenerateNormals();
#endif        


#ifdef MD5_USE_TANGENTS_BUMPMAPS
        target->GenerateTangents();
#endif

        /*
        Finally, as our vertex attributes data has changed, we must rebuffer the data to 
        graphics memory.
        */
        target->RebufferData();
    }
}


///*
//Rebuffers the vertex data on the graphics card. Now you know why we always keep hold of
//our vertex data in system memory! This function is actually entirely covered in the 
//skeletal animation tutorial text (unlike the other functions, which are kept as 
//pseudocode). This should be in the Mesh class really, as it's a useful function to have.
//It's worth pointing out that the glBufferSubData function never allocates new memory
//on the graphics card!
//*/
void MD5Mesh::RebufferData()    {
// BJ - TODO!
#if 0
    glBindBuffer(GL_ARRAY_BUFFER, bufferObject[VERTEX_BUFFER]);
    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector3), (void*)vertices);

    if(textureCoords) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TEXTURE_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector2), (void*)textureCoords);
    }

    //if (colours)    {
    if (colors)    {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[COLOUR_BUFFER]);
        //glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector4), (void*)colours);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector4), (void*)colors);
    }

#ifdef MD5_USE_NORMALS
    if(normals) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[NORMAL_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector3), (void*)normals);
    }
#endif

#ifdef MD5_USE_TANGENTS_BUMPMAPS
    if(tangents) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TANGENT_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector3), (void*)tangents);
    }
#endif

    if(indices) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject[INDEX_BUFFER]);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numVertices*sizeof(unsigned int), (void*)indices);
    }
#endif // end BJ TODO

    for ( u32 i = 0; i < m_meshBuffer->cnt_vertices; ++i )
    {
        Vectormath::Aos::Vector3& pos = vertices[i];
        Vector2& texCoord = textureCoords[i];
        m_meshBuffer->vertices[i] = S3DVertex( pos.getX(), pos.getY(), pos.getZ(),
                                               0.0f, 1.0f, 0.0f, // TODO - normals
                                               texCoord.u, 1.0f - texCoord.v );
    }
    
    for ( u32 i = 0; i < m_meshBuffer->cnt_indices; ++i )
    {
        m_meshBuffer->indices32[i] = indices[i];
    }
}

