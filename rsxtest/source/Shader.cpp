#include "Log.h"
#include "Shader.h"

//#define DBG_ASSERT(cond) assert(cond)
#define DBG_ASSERT(COND)

Shader::Shader() :
    m_context( nullptr ),
    m_vpo( nullptr ),
    m_fpo( nullptr ),
    m_vpUCode( nullptr ),
    m_fpUCode( nullptr ),
    m_fpBuffer( nullptr ),
    m_fpOffset( 0 )
{
}

Shader::~Shader()
{
}

bool Shader::Init( gcmContextData* context,
                   rsxVertexProgram* vpo,
                   rsxFragmentProgram* fpo )
{
    DBG_ASSERT( context != nullptr );
    DBG_ASSERT( vpo != nullptr );
    DBG_ASSERT( fpo != nullptr );

    m_context = context;
    m_vpo = vpo;
    m_fpo = fpo;

    bool res = InitUCode();

    return res;
}

bool Shader::InitUCode()
{
    u32 fpsize = 0;
    u32 vpsize = 0;

    rsxVertexProgramGetUCode( m_vpo, &m_vpUCode, &vpsize );
    DBG_LOG( "vpsize: %d\n", vpsize );

    rsxFragmentProgramGetUCode( m_fpo, &m_fpUCode, &fpsize );
    DBG_LOG( "fpsize: %d\n", fpsize );

    m_fpBuffer = (u32*)rsxMemalign( 64, fpsize );
    memcpy( m_fpBuffer, m_fpUCode, fpsize );
    rsxAddressToOffset( m_fpBuffer, &m_fpOffset );
    DBG_LOG( "fp buffer offset: %u\n", m_fpOffset );

    return true;
}

#undef DBG_ASSERT

