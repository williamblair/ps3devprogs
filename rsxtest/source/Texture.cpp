#include <cassert>
#include "Log.h"
#include "Texture.h"

#define DBG_ASSERT(cond) assert(cond)
//#define DBG_ASSERT(cond)

Texture::Texture() :
    m_context( nullptr ),
    m_buffer( nullptr ),
    m_offset( 0 )
{
}

Texture::~Texture()
{
}

bool Texture::Init( gcmContextData* context, u8* pixelData, u32 width, u32 height, u32 bytesPerPixel )
{
    m_context = context;
    m_width = width;
    m_height = height;
    
    m_buffer = (u32*)rsxMemalign( 128, width * height * 4 ); // force 4 bytes per pixel
    if ( !m_buffer ) {
        DBG_LOG( "failed to alloc tex buffer\n" );
        return false;
    }

    rsxAddressToOffset( m_buffer, &m_offset );
    
    DBG_ASSERT( bytesPerPixel == 4 || bytesPerPixel == 3 );
    DBG_LOG( "Texture bytes per pixel: %u\n", bytesPerPixel );
    u8* buffer = (u8*)m_buffer;
    for ( u32 i = 0; i < width * height * 4; i += 4 )
    {
        buffer[ i + 1 ] = *pixelData++; // r
        buffer[ i + 2 ] = *pixelData++; // g
        buffer[ i + 3 ] = *pixelData++; // b
        if ( bytesPerPixel == 4 ) {
            buffer[ i + 0 ] = *pixelData++; // a
        } else {
            buffer[ i + 0 ] = 255;
        }
    }

    return true;
}

void Texture::Set( u8 shaderTexUnit )
{
    u32 width = m_width;
    u32 height = m_height;
    u32 pitch = width * 4;

    DBG_ASSERT( m_buffer != nullptr );

    rsxInvalidateTextureCache( m_context, GCM_INVALIDATE_TEXTURE );
    
    m_texture.format       = (GCM_TEXTURE_FORMAT_A8R8G8B8 | GCM_TEXTURE_FORMAT_LIN);
    m_texture.mipmap       = 1;
    m_texture.dimension    = GCM_TEXTURE_DIMS_2D;
    m_texture.cubemap      = GCM_FALSE;
    m_texture.remap        = ((GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_B_SHIFT) |
                           (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_G_SHIFT) |
                           (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_R_SHIFT) |
                           (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_A_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_B << GCM_TEXTURE_REMAP_COLOR_B_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_G << GCM_TEXTURE_REMAP_COLOR_G_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_R << GCM_TEXTURE_REMAP_COLOR_R_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_A << GCM_TEXTURE_REMAP_COLOR_A_SHIFT));
    m_texture.width       = width;
    m_texture.height      = height;
    m_texture.depth       = 1;
    m_texture.location    = GCM_LOCATION_RSX;
    m_texture.pitch       = pitch;
    m_texture.offset      = m_offset;
    rsxLoadTexture(m_context,shaderTexUnit,&m_texture);
    rsxTextureControl(m_context,shaderTexUnit,GCM_TRUE,0<<8,12<<8,GCM_TEXTURE_MAX_ANISO_1);
    rsxTextureFilter(m_context,shaderTexUnit,0,GCM_TEXTURE_LINEAR,GCM_TEXTURE_LINEAR,GCM_TEXTURE_CONVOLUTION_QUINCUNX);
    //rsxTextureWrapMode(m_context,shaderTexUnit,GCM_TEXTURE_CLAMP_TO_EDGE,GCM_TEXTURE_CLAMP_TO_EDGE,GCM_TEXTURE_CLAMP_TO_EDGE,0,GCM_TEXTURE_ZFUNC_LESS,0);
    rsxTextureWrapMode(m_context,shaderTexUnit,GCM_TEXTURE_REPEAT,GCM_TEXTURE_REPEAT,GCM_TEXTURE_REPEAT,0,GCM_TEXTURE_ZFUNC_LESS,0);
}

#undef DBG_ASSERT

