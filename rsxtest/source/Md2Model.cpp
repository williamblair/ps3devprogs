#include "Md2Model.h"
#include "Log.h"
#include <fstream>
#include <iostream>

#define DECL_ANIM(name, startFrame, endFrame) \
    const Md2Model::Animation Md2Model::Animation::name = Md2Model::Animation(startFrame, endFrame)
#define DECL_ANIM_NOLOOP(name, startFrame, endFrame) \
    const Md2Model::Animation Md2Model::Animation::name = Md2Model::Animation(startFrame, endFrame, false)

// static member, start frame, end frame    
DECL_ANIM(Idle, 0, 39);
DECL_ANIM(Run, 40, 45);
DECL_ANIM(Attack, 46, 53);
DECL_ANIM(Pain1, 54, 57);
DECL_ANIM(Pain2, 58, 61);
DECL_ANIM(Pain3, 62, 65);
DECL_ANIM(Jump, 66, 71);
DECL_ANIM(FlipOff, 72, 83);
DECL_ANIM(Salute, 84, 94);
DECL_ANIM(Taunt, 95, 111);
DECL_ANIM(Wave, 112, 122);
DECL_ANIM(Point, 123, 134);
DECL_ANIM(CrouchIdle, 135, 153);
DECL_ANIM(CrouchWalk, 154, 159);
DECL_ANIM(CrouchAttack, 160, 168);
DECL_ANIM(CrouchPain, 169, 172);
DECL_ANIM(CrouchDeath, 173, 177);
DECL_ANIM_NOLOOP(Death1, 178, 183);
DECL_ANIM_NOLOOP(Death2, 184, 189);
DECL_ANIM_NOLOOP(Death3, 190, 197);

#undef DECL_ANIM
#undef DECL_ANIM_NOLOOP

Md2Model::Md2Model() :
    startFrame(0),
    endFrame(0),
    currentFrame(0),
    nextFrame(1),
    interpolation(0.0f)
{}
Md2Model::~Md2Model()
{}

bool Md2Model::Init( gcmContextData* context )
{
    return Entity::Init( context );
}
    
bool Md2Model::Load(const std::string& fileName)
{
    std::ifstream inFile(fileName, std::ios::binary);
    if (!inFile.is_open()) {
        DBG_LOG( "Failed to open %s\n", fileName.c_str() );
        return false;
    }
    
    Header header;
    inFile.read((char*)&header, sizeof(Header));
    
    // Verify file header correctness
    if (header.magic[0] != 'I' ||
        header.magic[1] != 'D' ||
        header.magic[2] != 'P' ||
        header.magic[3] != '2')
    {
        DBG_LOG( "Invalid header: magic != IDP2\n" );
        return false;
    }
    
    if ( lend2bend32( header.version ) != 8)
    {
        DBG_LOG( "Invalid header: version != 8: %08X\n", header.version );
        return false;
    }
    
    if ( !inFile ) {
        DBG_LOG( "Bad file after header read\n" );
        return false;
    }
    
    // reserve space for MD2 data
    skins.resize( lend2bend32( header.numSkins ) );
    texCoords.resize( lend2bend32( header.numTexCoords ) );
    md2TexCoords.resize( lend2bend32( header.numTexCoords ) );
    triangles.resize( lend2bend32( header.numTriangles ) );
    keyFrames.resize( lend2bend32( header.numFrames ) );
    
    for (size_t i = 0; i < keyFrames.size(); ++i)
    {
        keyFrames[i].vertices.resize( lend2bend32( header.numVertices ) );
        keyFrames[i].md2Vertices.resize( lend2bend32( header.numVertices ) );
    }
    
    DBG_LOG( "Keyframes size setvertices: %lu\n", keyFrames.size() );
    
    // read MD2 components
#define READ_DATA(offset, vec, number, type) \
    inFile.seekg(offset);                    \
    inFile.read((char*)vec.data(), number * sizeof(type))
    
    READ_DATA( lend2bend32( header.skinOffset ), skins, lend2bend32( header.numSkins ), Skin);    
    READ_DATA( lend2bend32( header.texCoordOffset ), md2TexCoords, lend2bend32( header.numTexCoords ), TexCoord);
    READ_DATA( lend2bend32( header.triangleOffset ), triangles, lend2bend32( header.numTriangles ), Triangle);

    for ( TexCoord& tc : md2TexCoords )
    {
        tc.s = lend2bend16( tc.s );
        tc.t = lend2bend16( tc.t );
    }
    for ( Triangle& tr : triangles )
    {
        tr.vertIndex[0] = lend2bend16( tr.vertIndex[0] );
        tr.vertIndex[1] = lend2bend16( tr.vertIndex[1] );
        tr.vertIndex[2] = lend2bend16( tr.vertIndex[2] );
        tr.texCoordIndex[0] = lend2bend16( tr.texCoordIndex[0] );
        tr.texCoordIndex[1] = lend2bend16( tr.texCoordIndex[1] );
        tr.texCoordIndex[2] = lend2bend16( tr.texCoordIndex[2] );
    }

#undef READ_DATA
    
#if 0
    {
        int32_t newPos = lend2bend32( header.frameOffset );
        inFile.seekg( newPos );
        long unsigned int curPos = inFile.tellg();
        if ( curPos != lend2bend32( header.frameOffset ) ) {
            DBG_LOG( "Failed to seekg: %lu\n", curPos );
            return false;
        }
    }
#endif
    
    for (int i = 0; i < lend2bend32( header.numFrames ); ++i)
    {
        KeyFrame* f = &keyFrames[i];
        inFile.read((char*)(f->scale), 3 * sizeof(f32));
        if (!inFile) { DBG_LOG( "Failed to read\n" ); return false;}
        f->scale[0] = lend2bend32f( f->scale[0] );
        f->scale[1] = lend2bend32f( f->scale[1] );
        f->scale[2] = lend2bend32f( f->scale[2] );

        inFile.read((char*)(f->translate), 3 * sizeof(float));
        if (!inFile) { DBG_LOG( "Failed to read\n" ); return false;}
        f->translate[0] = lend2bend32f( f->translate[0] );
        f->translate[1] = lend2bend32f( f->translate[1] );
        f->translate[2] = lend2bend32f( f->translate[2] );

        inFile.read((char*)(f->name), 16 * sizeof(char));
        if (!inFile) { DBG_LOG( "Failed to read\n" ); return false;}

        inFile.read((char*)(f->md2Vertices.data()), lend2bend32( header.numVertices ) * sizeof(Vertex));
        if (!inFile) { DBG_LOG( "Failed to read\n" ); return false;}
    }
    
    // scale MD2 vertices into regular OpenGL vertices
    //radii.clear();
    for (KeyFrame& frame : keyFrames)
    {
        float min = 10000.0f;
        float max = -10000.0f;
        
        int k = 0;
        for (Vector3& vertex : frame.vertices)
        {
            vertex.setX( frame.scale[0] * frame.md2Vertices[k].v[0] + frame.translate[0] );
            vertex.setZ( frame.scale[1] * frame.md2Vertices[k].v[1] + frame.translate[1] );
            vertex.setY( frame.scale[2] * frame.md2Vertices[k].v[2] + frame.translate[2] );
                       
            ++k;
            
            if (vertex.getY() < min) min = vertex.getY();
            if (vertex.getY() > max) max = vertex.getY();
        }
        
        //float frameRadius = (max - min) / 2.0f;
        //radii.push_back(frameRadius);
    }
        
    // scale tex coords into regular OpenGL tex coords
    int i = 0;
    for (Vector2& texCoord : texCoords)
    {
        texCoord.u = float( md2TexCoords[i].s ) / 
                     float( lend2bend32( header.skinWidth ) );
        texCoord.v = 1.0f - (float( md2TexCoords[i].t ) / 
                             float( lend2bend32( header.skinHeight )));
        ++i;
    }
    
    inFile.close();
    
    reorganizeVertices();
    stripTextureNames();
    
    interpolatedFrame.vertices = keyFrames[0].vertices;
    currentAnimName = std::string(keyFrames[0].name);
    DBG_LOG( "interpolated frame vertices size: %lu\n", interpolatedFrame.vertices.size() );
        
    return genBuffers();
}

void Md2Model::Update( float dt )
{
    // regenerate the model matrix
    Entity::Update( dt );
    
    const float FRAMES_PER_SECOND = 8.0f;
    interpolation += ( dt / 1000.0f ) * FRAMES_PER_SECOND;
    if (interpolation >= 1.0f)
    {
        currentFrame = nextFrame++;
        if (nextFrame > endFrame)
        {
            if (loopAnim)
            {
                nextFrame = startFrame;
            }
            else
            {
                nextFrame = endFrame;
                startFrame = endFrame;
            }
        }
        
        interpolation = 0.0f;
    }
    
    float t = interpolation;
    int i = 0;
    for (Vector3& vertex : interpolatedFrame.vertices)
    {
#define LERP(a, b, t) (a) + ((t) * ((b) - (a)))
        
        float x1 = keyFrames[currentFrame].vertices[i].getX();
        float x2 = keyFrames[nextFrame].vertices[i].getX();
        vertex.setX( LERP(x1, x2, t) );
        
        float y1 = keyFrames[currentFrame].vertices[i].getY();
        float y2 = keyFrames[nextFrame].vertices[i].getY();
        vertex.setY( LERP(y1, y2, t) );
        
        float z1 = keyFrames[currentFrame].vertices[i].getZ();
        float z2 = keyFrames[nextFrame].vertices[i].getZ();
        vertex.setZ( LERP(z1, z2, t) );
        
#undef LERP
        ++i;
    }

    if ( interpolatedFrame.vertices.size() != m_meshBuffer->cnt_vertices ) {
        DBG_LOG( "Interp verts size != cnt_vertices\n" );
        return;
    }
    
    // Update RSX memory
    for ( u32 i = 0; i < m_meshBuffer->cnt_vertices; ++i )
    {
        Vector3& pos = interpolatedFrame.vertices[i];
        m_meshBuffer->vertices[i] = S3DVertex( pos.getX(), pos.getY(), pos.getZ(),
                                               0.0f, 1.0f, 0.0f, // TODO - normals
                                               texCoords[i].u, texCoords[i].v );
    }
}

// Because some vertices are reused, different texture indices
// by default can refer to the same vertex indices, so we copy these
// vertices so there are no conflictions
void Md2Model::reorganizeVertices()
{
    std::vector<Vector3> tmpVertices;
    std::vector<Vector2> tmpTexCoords;
    
    bool texCoordsDone = false;
    
    for (KeyFrame& frame : keyFrames)
    {
        tmpVertices.clear();
        
        for (uint32_t i = 0; i < triangles.size(); ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                tmpVertices.push_back(frame.vertices[triangles[i].vertIndex[j]]);
                
                // only copy the texture coords once
                if (!texCoordsDone)
                {
                    tmpTexCoords.push_back(texCoords[triangles[i].texCoordIndex[j]]);
                }
            }
        }
        
        // tex coords shared between frames; only copy once
        texCoordsDone = true;
        
        // copy the reassigned vertex data
        frame.vertices = tmpVertices;
    }
    
    // copy the reassigned tex coord
    texCoords = tmpTexCoords;
}

// remove any folder prefixes from the model texture file names
void Md2Model::stripTextureNames()
{
    for (Skin& skin : skins)
    {
        std::string texture = skin.name;
        
        size_t fileNameStart = texture.find_last_of("/") + 1;
        size_t lastDot = texture.find_last_of(".");
        
        std::string textureName = texture.substr(fileNameStart, lastDot - fileNameStart);
        texNames.push_back(textureName);
    }
}

bool Md2Model::genBuffers()
{
    DBG_LOG( "Interpolated frame vertices size: %lu\n",
            interpolatedFrame.vertices.size() );
    m_meshBuffer->cnt_vertices = interpolatedFrame.vertices.size();
    m_meshBuffer->vertices =
        (S3DVertex*)rsxMemalign( 128,
                                 m_meshBuffer->cnt_vertices * sizeof(S3DVertex) );
    if ( !m_meshBuffer->vertices ) {
        DBG_LOG( "Failed to alloc vertices\n" );
        return false;
    }
    m_meshBuffer->indices = nullptr;
    if ( texCoords.size() != interpolatedFrame.vertices.size() ) {
        DBG_LOG( "Tex coords size != vertices size (%lu, %lu)\n",
                texCoords.size(), interpolatedFrame.vertices.size() );
        return false;
    }
    for ( u32 i = 0; i < m_meshBuffer->cnt_vertices; ++i )
    {
        Vector3& pos = interpolatedFrame.vertices[i];
        m_meshBuffer->vertices[i] = S3DVertex( pos.getX(), pos.getY(), pos.getZ(),
                                               0.0f, 1.0f, 0.0f, // TODO - normals
                                               texCoords[i].u, texCoords[i].v );
    }
    return true;
}

