#include "Cube.h"

Cube::Cube()
{
}

Cube::~Cube()
{
}

bool Cube::Init( gcmContextData* context )
{
    if ( !Entity::Init( context ) ) {
        return false;
    }

    // TODO - configure
    float fsize = 5.0f;
    return createCube( fsize );
}

bool Cube::createCube( f32 size )
{
    u32 i;
    const u16 u[36] = {   0,1,2,   0,2,3,   1,4,5,   1,5,2,   4,7,6,     4,6,5, 
                          7,0,3,   7,3,6,   9,2,5,   9,5,8,   0,10,11,   0,7,10};

    m_meshBuffer->cnt_indices = 36;
    m_meshBuffer->indices = (u16*)rsxMemalign(128,m_meshBuffer->cnt_indices*sizeof(u16));

    for(i=0;i<36;i++) m_meshBuffer->indices[i] = u[i];

    m_meshBuffer->cnt_vertices = 12;
    m_meshBuffer->vertices = (S3DVertex*)rsxMemalign(128,m_meshBuffer->cnt_vertices*sizeof(S3DVertex));

    m_meshBuffer->vertices[0] = S3DVertex(0,0,0, -1,-1,-1, 1, 0);
    m_meshBuffer->vertices[1] = S3DVertex(1,0,0,  1,-1,-1, 1, 1);
    m_meshBuffer->vertices[2] = S3DVertex(1,1,0,  1, 1,-1, 0, 1);
    m_meshBuffer->vertices[3] = S3DVertex(0,1,0, -1, 1,-1, 0, 0);
    m_meshBuffer->vertices[4] = S3DVertex(1,0,1,  1,-1, 1, 1, 0);
    m_meshBuffer->vertices[5] = S3DVertex(1,1,1,  1, 1, 1, 0, 0);
    m_meshBuffer->vertices[6] = S3DVertex(0,1,1, -1, 1, 1, 0, 1);
    m_meshBuffer->vertices[7] = S3DVertex(0,0,1, -1,-1, 1, 1, 1);
    m_meshBuffer->vertices[8] = S3DVertex(0,1,1, -1, 1, 1, 1, 0);
    m_meshBuffer->vertices[9] = S3DVertex(0,1,0, -1, 1,-1, 1, 1);
    m_meshBuffer->vertices[10] = S3DVertex(1,0,1,  1,-1, 1, 0, 1);
    m_meshBuffer->vertices[11] = S3DVertex(1,0,0,  1,-1,-1, 0, 0);

    for(i=0;i<12;i++) {
        m_meshBuffer->vertices[i].pos -= Vector3(0.5f,0.5f,0.5f);
        m_meshBuffer->vertices[i].pos *= size;
    }

    return true;
}


