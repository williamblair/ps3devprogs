#include "Renderer.h"

Renderer::Renderer() :
    m_hostAddr( nullptr ),
    m_context( nullptr )
{
}

Renderer::~Renderer()
{
}

bool Renderer::Init()
{
    m_hostAddr = memalign( HOST_ADDR_ALIGNMENT, HOSTBUFFER_SIZE );
    init_screen( m_hostAddr, HOSTBUFFER_SIZE );
    m_context = context; // rsxutil.h
    SetDrawEnv();
    setRenderTarget( curr_fb );
    return true;
}

void Renderer::SetDrawEnv()
{
    rsxSetColorMask(context,GCM_COLOR_MASK_B |
                            GCM_COLOR_MASK_G |
                            GCM_COLOR_MASK_R |
                            GCM_COLOR_MASK_A);

    rsxSetColorMaskMrt(context,0);

    u16 x,y,w,h;
    f32 min, max;
    f32 scale[4],offset[4];

    x = 0;
    y = 0;
    w = display_width;
    h = display_height;
    min = 0.0f;
    max = 1.0f;
    scale[0] = w*0.5f;
    scale[1] = h*-0.5f;
    scale[2] = (max - min)*0.5f;
    scale[3] = 0.0f;
    offset[0] = x + w*0.5f;
    offset[1] = y + h*0.5f;
    offset[2] = (max + min)*0.5f;
    offset[3] = 0.0f;

    rsxSetViewport(context,x, y, w, h, min, max, scale, offset);
    rsxSetScissor(context,x,y,w,h);

    rsxSetDepthTestEnable(context,GCM_TRUE);
    rsxSetDepthFunc(context,GCM_LESS);
    rsxSetShadeModel(context,GCM_SHADE_MODEL_SMOOTH);
    rsxSetDepthWriteEnable(context,1);
    rsxSetFrontFace(context,GCM_FRONTFACE_CCW);
    rsxSetBlendEnable( context, GCM_TRUE ); // enable blending
    rsxSetBlendEquation( context, GCM_FUNC_ADD, GCM_FUNC_ADD ); // default blend equation...
    rsxSetBlendFunc( context, GCM_SRC_ALPHA, // sfcolor (source)
                              GCM_ONE_MINUS_SRC_ALPHA, // dfcolor (destination)
                              GCM_SRC_ALPHA, // sfalpha
                              GCM_ONE_MINUS_SRC_ALPHA ); // dfalpha
}

void Renderer::BeginFrame()
{
    u32 i;
    u32 color = 0; // black screen clear
    SetDrawEnv();
    rsxSetClearColor(context,color);
    rsxSetClearDepthStencil(context,0xffffff00);
    rsxClearSurface(context,GCM_CLEAR_R |
                            GCM_CLEAR_G |
                            GCM_CLEAR_B |
                            GCM_CLEAR_A |
                            GCM_CLEAR_S |
                            GCM_CLEAR_Z);

    rsxSetZMinMaxControl(context,0,1,1);

    for(i=0;i<8;i++)
        rsxSetViewportClip(context,i,display_width,display_height);
}

void Renderer::RenderEntity( Entity& entity )
{
    rsxSetUserClipPlaneControl(context,GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE);
    entity.Render();
}


