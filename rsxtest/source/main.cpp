#include <fstream>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>
#include <sys/systime.h>

#include <io/pad.h>
#include <rsx/rsx.h>

#include "mesh.h"

#include "Shader.h"
#include "Texture.h"
#include "Entity.h"
#include "Sphere.h"
#include "Torus.h"
#include "Cube.h"
#include "Log.h"
#include "Pad.h"
#include "Renderer.h"
#include "Md2Model.h"
#include "MD5FileData.h"
#include "MD5Node.h"
#include "TargaImage.h"
#include "GameTimer.h"
#include "Terrain.h"
#include "Tree.h"
#include "OrbitCamera.h"
#include "Vector2.h"

#include "default_shader_vpo.h"
#include "default_shader_fpo.h"
rsxVertexProgram *vpo = (rsxVertexProgram*)default_shader_vpo;
rsxFragmentProgram *fpo = (rsxFragmentProgram*)default_shader_fpo;

#define DEGTORAD(a)            ( (a) *  0.01745329252f )
#define RADTODEG(a)            ( (a) * 57.29577951f )

u32 running = 0;

static Renderer g_Renderer;
static Shader g_Shader;
static Pad g_Pad;
static MD5FileData g_MD5Data;
static MD5Node* g_MD5Node = nullptr;
static Vector3 g_MD5Position(0.0f, 0.0f, 0.0f);
static Targa::Image g_MD5Texture;
static Targa::Image g_TerrainTexture;
static Targa::Image g_TreeTexture;
static Terrain g_Terrain;
static Tree g_Tree;
static GameTimer g_Timer;
static OrbitCamera g_Camera;

static Matrix4 g_ProjMatrix;
static bool g_RenderLog = true;

SYS_PROCESS_PARAM(1001, 0x100000); // what is this??

extern "C" {
static void program_exit_callback()
{
    gcmSetWaitFlip(g_Renderer.GetGcmContext());
    rsxFinish(g_Renderer.GetGcmContext(),1);
}

static void sysutil_exit_callback(u64 status,u64 param,void *usrdata)
{
    switch(status) {
        case SYSUTIL_EXIT_GAME:
            running = 0;
            break;
        case SYSUTIL_DRAW_BEGIN:
        case SYSUTIL_DRAW_END:
            break;
        default:
            break;
    }
}
} // end extern "C"

// input range 0-255, middle = 127
Vector2 Filter2D( int inputX, int inputY )
{
    const float deadZone = (8000.0f/32768.0f) * 127.0f;
    const float maxValue = (30000.0f/32768.0f) * 127.0f;
    
    Vector2 dir;
    dir.x = (float)(inputX) - 127.0f;
    dir.y = (float)(inputY) - 127.0f;
    
    float length = dir.Length();
    
    if ( length < deadZone )
    {
        dir.x = 0.0f;
        dir.y = 0.0f;
    }
    else
    {
        // calculate fractional between dead zone and max value circles
        float f = ( length - deadZone ) / ( maxValue - deadZone );
        
        // clamp between 0 and 1
        if ( f < 0.0f ) { f = 0.0f; }
        if ( f > 1.0f ) { f = 1.0f; }
        
        // normalize vector and scale it to the fractional value
        dir.x *= f / length;
        dir.y *= f / length;
    }    
    
    return dir;
}

void drawFrame()
{
    Matrix4 rotX,rotY;
    Matrix4 modelMatrix,modelMatrixIT,modelViewMatrix;
    static f32 rot = 0.0f;

    g_Shader.Use();
    u8 texIndex = g_Shader.GetFragmentProgAttrib( "texture" )->index;
    g_TerrainTexture.Set( texIndex );
    
    g_Shader.SetVertexProgParam( "projMatrix", (float*)&g_ProjMatrix );
    
    modelMatrix = Matrix4::scale( Vector3( 1.0f, 1.0f, 1.0f ) );
    modelMatrix.setTranslation( Vector3( 0.0f, 0.0f, 0.0f ) );

    modelMatrixIT = inverse(modelMatrix);
    modelViewMatrix = transpose(g_Camera.GetViewMatrix()*modelMatrix);

    g_Shader.SetVertexProgParam( "modelViewMatrix", (float*)&modelViewMatrix );
    
    g_Renderer.RenderEntity( g_Terrain );

    g_MD5Texture.Set( texIndex );

    modelMatrixIT = inverse( g_MD5Node->GetModelMatrix() );
    modelViewMatrix = transpose( g_Camera.GetViewMatrix() *
                                 g_MD5Node->GetModelMatrix() );

    g_Shader.SetVertexProgParam( "modelViewMatrix", (float*)&modelViewMatrix );
    
    g_Renderer.RenderEntity( *g_MD5Node );

    modelMatrix = Matrix4::scale( Vector3( 5.0f, 5.0f, 5.0f ) );
    modelMatrix.setTranslation( Vector3( -10.0f, 5.0f, 0.0f ) );

    modelMatrixIT = inverse(modelMatrix);
    modelViewMatrix = transpose(g_Camera.GetViewMatrix()*modelMatrix);

    g_Shader.SetVertexProgParam( "modelViewMatrix", (float*)&modelViewMatrix );
    g_TreeTexture.Set( texIndex );
    
    g_Renderer.RenderEntity( g_Tree );

    rot += 0.1f;
    if(rot >= 360.0f) rot = fmodf(rot, 360.0f);
}

static void testActuators()
{
    // large actuator
    static unsigned short algVal;
    algVal = g_Pad.GetLeftAnalogY();
    static unsigned short prevAlgVal = algVal;
    if ( algVal != prevAlgVal )
    {
        if ( algVal > prevAlgVal  && 
             algVal - prevAlgVal > 5 ) {
            g_Pad.SetLargeActuator( algVal );
        } else if ( algVal < prevAlgVal &&
                    prevAlgVal - algVal > 5 ) {
            g_Pad.SetLargeActuator( algVal );
        }
        DBG_LOG( "Alg X: %u", algVal );
    }
    prevAlgVal = algVal;

    // small actuator
    //if ( g_Pad.IsClicked( Pad::SQUARE ) )
    //{
    //    static bool actVal = false;
    //    actVal = !actVal;
    //    g_Pad.SetSmallActuator( actVal );
    //}
}

static bool loadFiles()
{

#define EXIT_ON_FAIL( func, failMsg, msg ) \
    if ( !func ) { \
        DBG_LOG( failMsg ); \
        return false; \
    } \
    DBG_LOG( msg )

    std::string line;
    std::ifstream testFile( ASSETS_DIR"/test.txt" );
    if ( !testFile.is_open() ) {
        DBG_LOG( "Failed to open assets/test.txt\n" );
        return false;
    } else {
        for (; std::getline( testFile, line ); )
        {
            DBG_LOG( line.c_str() );
        }
    }

    EXIT_ON_FAIL( g_MD5Texture.Load( g_Renderer.GetGcmContext(),
                             ASSETS_DIR"/models/hellknight/hellknight.tga" ),
                  "Failed to load md5texture\n",
                  "Success loading md5texture\n" );
    EXIT_ON_FAIL( g_MD5Data.Init( g_Renderer.GetGcmContext(),
                          ASSETS_DIR"/models/hellknight/hellknight.md5mesh",
                          1.0f/20.0f ),
                  "Failed to load hellknight md5\n",
                  "Sucess load hellknight md5\n" );
    EXIT_ON_FAIL( (g_MD5Node = new MD5Node( g_MD5Data )),
                  "Failed to create md5node\n",
                  "Sucess creating md5node\n" );
    EXIT_ON_FAIL( g_MD5Data.AddAnim( ASSETS_DIR"/models/hellknight/idle2.md5anim" ),
                  "Failed to load md5 anim\n",
                  "Sucess loading md5 anim\n" );
    EXIT_ON_FAIL( g_MD5Data.AddAnim( ASSETS_DIR"/models/hellknight/walk7.md5anim" ),
                  "Failed to load md5 anim\n",
                  "Sucess loading md5 anim\n" );
    //g_MD5Node->PlayAnim( ASSETS_DIR"/models/hellknight/idle2.md5anim" );
    g_MD5Node->PlayAnim( ASSETS_DIR"/models/hellknight/walk7.md5anim" );
    g_MD5Node->SetPosition( Vector3( 0.0f, 5.0f, 0.0f ) ); // move up a little above terrain
    EXIT_ON_FAIL( g_Terrain.Init( g_Renderer.GetGcmContext(),
                          ASSETS_DIR"/heightmap.raw",
                          65,     // width
                          5.0f ), // heightScale
                  "Failed to init g_Terrain\n",
                  "Sucess init g_Terrain\n" );
    EXIT_ON_FAIL( g_TerrainTexture.Load( g_Renderer.GetGcmContext(),
                                 ASSETS_DIR"/grass.tga" ),
                  "Failed to load g_Terrain texture\n",
                  "Success init g_Terrain texture\n" );
    EXIT_ON_FAIL( g_TreeTexture.Load( g_Renderer.GetGcmContext(),
                              ASSETS_DIR"/beech.tga" ),
                  "Failed to load g_TreeTexture\n",
                  "Success init g_TreeTexture\n" );

    EXIT_ON_FAIL( g_Tree.Init( g_Renderer.GetGcmContext() ),
                  "Failed to init tree\n",
                  "Sucess init tree\n" );

#undef EXIT_ON_FAIL
    
    return true;
}

void handleInput()
{
    // Move character with left analog
    Vector2 leftAnalogDir = Filter2D( g_Pad.GetLeftAnalogX(),
                                      g_Pad.GetLeftAnalogY() );
    float dirLen = leftAnalogDir.Length();
    g_MD5Node->SetForwardSpeed( -dirLen * 0.005f );
    static bool isIdle = false;
    if ( dirLen > 0.000001f ) {
        float angle = atan2( -leftAnalogDir.y, leftAnalogDir.x );
        g_MD5Node->SetYaw( angle );
        if ( isIdle ) {
            g_MD5Node->PlayAnim( ASSETS_DIR"/models/hellknight/walk7.md5anim" );
            isIdle = false;
        }

    } else if ( !isIdle ) {
        g_MD5Node->PlayAnim( ASSETS_DIR"/models/hellknight/idle2.md5anim" );
        isIdle = true;
    }

    // Rotate camera and character with right analog
    Vector2 rightAnalogDir = Filter2D( g_Pad.GetRightAnalogX(),
                                       g_Pad.GetRightAnalogY() );
    g_Camera.SetPitchSpeed( rightAnalogDir.y * 0.005f );
    g_Camera.SetYawSpeed( rightAnalogDir.x * 0.005f );
    g_MD5Node->SetYawOffset( g_Camera.GetYaw() );
    
}

int main(int argc,const char *argv[])
{
    printf("rsxtest started...\n");

#define EXIT_ON_FAIL( funcCall, name )          \
    if ( !funcCall ) {                          \
        DBG_LOG( "Failed to init %s\n", name ); \
        goto done;                              \
    }                                           \
    else {                                      \
        DBG_LOG( "%s init success\n", name );   \
    }

    EXIT_ON_FAIL( g_Renderer.Init(), "g_Renderer" );
    EXIT_ON_FAIL( g_Pad.Init( 0 ), "g_Pad" );
    EXIT_ON_FAIL( g_Shader.Init( g_Renderer.GetGcmContext(),
                                   vpo, fpo ),
                                   "g_Shader" );

    DebugFont::init();
    DebugFont::setScreenRes( g_Renderer.GetWidth(), g_Renderer.GetHeight() );

    atexit(program_exit_callback);
    sysUtilRegisterCallback(0,sysutil_exit_callback,NULL);

    g_ProjMatrix = transpose( Matrix4::perspective( DEGTORAD( 45.0f ),
                             (float)g_Renderer.GetWidth() /
                             (float)g_Renderer.GetHeight(),
                             1.0f,10000.0f) );

    EXIT_ON_FAIL( loadFiles(), "loadFiles" );

#undef EXIT_ON_FAIL

    running = 1;
    while(running) {
        sysUtilCheckCallback();

        g_Pad.Update();
        if ( g_Pad.IsHeld( Pad::CROSS ) ) {
            goto done;
        }
        if ( g_Pad.IsClicked( Pad::CIRCLE ) ) {
            g_RenderLog = !g_RenderLog;
        }

        //testActuators();

        g_Renderer.BeginFrame();
        
        drawFrame();

        if ( g_RenderLog )  {
            Log::DebugRender( 10, 10, 1.0f, 0.0f, 0.0f );
        }

        {
            static char fpsStr[75];
            sprintf( fpsStr, "FPS: %f\n", g_Timer.GetFPS() );
            DebugFont::setColor( 0.0f, 1.0f, 0.0f, 1.0f ); // green
            DebugFont::setPosition( 10, (g_Renderer.GetHeight() - 80) );
            DebugFont::print( fpsStr );
        }

        g_Renderer.EndFrame();

        g_Timer.Update();
        
        handleInput();
        
        g_MD5Node->Update( g_Timer.GetDeltaMS() );
        g_Camera.SetTarget( g_MD5Node->GetPosition() );
        g_Camera.Update( g_Timer.GetDeltaMS() );
    }

done:
    if ( g_MD5Node != nullptr ) {
        DBG_LOG( "Deleting MD5Node\n" );
        delete g_MD5Node;
        g_MD5Node = nullptr;
    }
    printf("rsxtest done...\n");
    DebugFont::shutdown();
    program_exit_callback();
    return 0;
}

