#ifndef TORUS_H_INCLUDED
#define TORUS_H_INCLUDED

#include "Entity.h"

class Torus : public Entity
{
public:
    Torus();
    virtual ~Torus();

    virtual bool Init( gcmContextData* context );
    
private:

    bool createTorus( f32 outerRadius, f32 innerRadius, u32 polyCntX, u32 polyCntY );
};

#endif // TORUS_H_INCLUDED

