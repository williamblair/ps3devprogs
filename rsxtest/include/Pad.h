#ifndef PAD_H_INCLUDED
#define PAD_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

class Pad
{
public:

    Pad();
    ~Pad();

    enum Button
    {
        LEFT = 0,
        DOWN,
        RIGHT,
        UP,
        SQUARE,
        CROSS,
        CIRCLE,
        TRIANGLE
    };

    bool Init( int padnum )
    {
        if ( !m_sysPadInitted ) {
            // TODO - look into this function
            ioPadInit(7);
            m_sysPadInitted = true;
        }
        m_padnum = padnum;
        return true;
    }

    void Update();
    bool IsHeld( Button b );
    bool IsClicked( Button b );
    unsigned short GetLeftAnalogX() { return m_paddata.ANA_L_H; }
    unsigned short GetLeftAnalogY() { return m_paddata.ANA_L_V; }
    unsigned short GetRightAnalogX() { return m_paddata.ANA_R_H; }
    unsigned short GetRightAnalogY() { return m_paddata.ANA_R_V; }

    bool SetSmallActuator( bool isOn ); // either on or off
    bool SetLargeActuator( unsigned char val ); // 0-255

private:
    static bool m_sysPadInitted;
    padInfo m_padinfo;
    padData m_paddata;
    padData m_prevPadData;
    bool m_isActive;
    padActParam m_actparam;
    int m_padnum;
};

#endif // PAD_H_INCLUDED

