#ifndef ENTITY_H_INCLUDED
#define ENTITY_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

#include "mesh.h"
#include "rsxutil.h"

class Entity
{
public:
    Entity();
    virtual ~Entity();

    // override this, though be sure to call Entity::Init() also!
    virtual bool Init( gcmContextData* context );

    virtual void Render();

    void SetYawSpeed( float speed ) { m_yawSpeed = speed; }
    void SetYawOffsetSpeed( float speed ) { m_yawOffsetSpeed = speed; }
    float GetYawSpeed() const { return m_yawSpeed; }
    void SetYaw( float yaw ) { m_yaw = yaw; }
    void AddYaw( float yaw ) { m_yaw += yaw; }
    void SetYawOffset( float yaw ) { m_yawOffset = yaw; }
    void AddYawOffset( float yaw ) { m_yawOffset += yaw; }
    float GetYaw() const { return m_yaw; }
    void SetForwardSpeed( float speed ) { m_forwardSpeed = speed; }
    float GetForwardSpeed() const { return m_forwardSpeed; }

    // generates model matrix based on position and rotation
    // dt = delta time in milliseconds
    virtual void Update( float dt );
    Vectormath::Aos::Matrix4& GetModelMatrix() { return m_modelMat; }

    void SetPosition( Vectormath::Aos::Vector3 pos ) { m_position = pos; }
    Vectormath::Aos::Vector3& GetPosition() { return m_position; }    

protected:

    gcmContextData* m_context;
    SMeshBuffer* m_meshBuffer;

    Vectormath::Aos::Matrix4 m_modelMat;
    Vectormath::Aos::Vector3 m_position;
    float m_yawSpeed; // added to yaw each update times dt
    float m_yaw; // yaw in radians
    float m_yawOffset; // additional yaw to rotate the forward vector; e.g. by a camera
    float m_yawOffsetSpeed; // added to yawOffset each update times dt
    
    float m_pitchSpeed; // added to pitch each update times dt
    float m_pitch; // pitch in radians
    
    float m_forwardSpeed; // added to m_position based on calculated forward vec
};

#endif // ENTITY_H_INCLUDED

