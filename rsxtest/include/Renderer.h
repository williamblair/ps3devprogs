#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

#include "mesh.h"
#include "rsxutil.h"
#include "Entity.h"

class Renderer
{
public:
    Renderer();
    ~Renderer();
    
    bool Init();

    void SetDrawEnv();

    void BeginFrame();
    void EndFrame() { flip(); }

    void RenderEntity( Entity& entity );

    gcmContextData* GetGcmContext() { return m_context; }

    u32 GetWidth() { return display_width; }
    u32 GetHeight() { return display_height; }

private:
    void* m_hostAddr;
    gcmContextData* m_context;
};

#endif

