#ifndef GAME_TIMER_H_INCLUDED
#define GAME_TIMER_H_INCLUDED

#include <fstream>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>
#include <sys/systime.h>

class GameTimer
{
public:
    GameTimer()
    {
        GetCurSecNsec( &m_lastSec, &m_lastNsec );
        m_FPS = 0.0f;
    }
    ~GameTimer()
    {}
    void Update()
    {
        u64 curSec;
        u64 curNsec;
        GetCurSecNsec( &curSec, &curNsec );

        //u64 secDiff = curSec - m_lastSec;
        //u64 nsecDiff = (curNsec < m_lastNsec) ? m_lastNsec - curNsec : curNsec - m_lastNsec;
        u64 curNano = (curSec * 1e9) + curNsec;
        u64 lastNano = (m_lastSec * 1e9) + m_lastNsec;

        //m_deltaMs = (float( secDiff ) * 1000.0f) +
        //            (float( nsecDiff ) / 1000000.0f);
        m_deltaMs = float((curNano - lastNano) / 1000000);

        m_lastSec = curSec;
        m_lastNsec = curNsec;

        ++m_framesCount;
        m_fpsTimeCount += m_deltaMs; 
        if ( m_fpsTimeCount / 1000.0f >= m_fpsDuration )
        {
            m_FPS = float( m_framesCount ) / ( m_fpsTimeCount / 1000.0f );
            m_framesCount = 0;
            m_fpsTimeCount = 0.0f;
        }
    }
    float GetDeltaMS() { return m_deltaMs; }
    float GetFPS() { return m_FPS; }
private:
    float m_deltaMs; // time passed since last update
    float m_FPS;
    u64 m_lastSec;
    u64 m_lastNsec;
    u32 m_framesCount = 0;
    float m_fpsTimeCount = 0.0f;
    const float m_fpsDuration = 1.0f; // seconds until update fps

    inline void GetCurSecNsec( u64* sec, u64* nsec )
    {
        sysGetCurrentTime( sec, nsec );
    }
};

#endif // GAME_TIMER_H_INCLUDED
