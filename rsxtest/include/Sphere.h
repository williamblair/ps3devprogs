#ifndef SPHERE_H_INCLUDED
#define SPHERE_H_INCLUDED

#include "Entity.h"

class Sphere : public Entity
{
public:
    Sphere();
    virtual ~Sphere();

    virtual bool Init( gcmContextData* context );
    
private:

    bool createSphere( f32 radius, u32 polyCntX, u32 polyCntY );
};

#endif // SPHERE_H_INCLUDED

