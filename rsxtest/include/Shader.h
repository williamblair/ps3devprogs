#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ppu-types.h>
#include <sys/process.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

#include "rsxutil.h"

class Shader
{
public:
    Shader();
    ~Shader();

    bool Init( gcmContextData* context,
               rsxVertexProgram* vpo, 
               rsxFragmentProgram* fpo );

    void Use() {
        rsxLoadVertexProgram( m_context, m_vpo, m_vpUCode );
        rsxLoadFragmentProgramLocation( m_context, m_fpo, m_fpOffset, GCM_LOCATION_RSX );
    }

    rsxProgramAttrib* GetFragmentProgAttrib( const char* name )
    {
        return rsxFragmentProgramGetAttrib( m_fpo, name );
    }

    void SetVertexProgParam( const char* name, float* val )
    {
        rsxProgramConst* prgCnst = rsxVertexProgramGetConst( m_vpo, name );
        if ( prgCnst == nullptr )
        {
            printf( "%s:%d: failed to get vertex prog const: %s\n",
                    __FILE__,
                    __LINE__,
                    name );
        }
        else
        {
            rsxSetVertexProgramParameter( m_context, m_vpo, prgCnst, val );
        }
    }

private:

    gcmContextData* m_context;
    rsxVertexProgram* m_vpo;
    rsxFragmentProgram* m_fpo;
    
    void* m_vpUCode;
    void* m_fpUCode;

    u32* m_fpBuffer;
    u32  m_fpOffset;

    bool InitUCode();
};

#endif // SHADER_H_INCLUDED

