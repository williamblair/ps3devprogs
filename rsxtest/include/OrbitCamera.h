#ifndef ORBIT_CAMERA_H_INCLUDED
#define ORBIT_CAMERA_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>
#include <sys/systime.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <vectormath/cpp/vectormath_aos.h>

/*
 * Based on Orbit Camera from "Game Programming in C++"
 * by Sanjay Madhav
 * https://github.com/gameprogcpp/code
 */
class OrbitCamera
{
public:

    OrbitCamera();
    ~OrbitCamera();
    
    void SetTarget( Vectormath::Aos::Vector3& target ) { m_target = target; }
    void SetPitchSpeed( const float speed ) { m_pitchSpeed = speed; }
    void SetYawSpeed( const float speed ) { m_yawSpeed = speed; }
    float GetYaw() const { return m_yaw; }
    
    void Update( const float dt ); // dt = delta time in milliseconds
    
    Vectormath::Aos::Matrix4& GetViewMatrix() { return m_viewMat; }

private:
    Vectormath::Aos::Vector3 m_offset; // offset from target
    Vectormath::Aos::Vector3 m_up; // up vector of camera
    Vectormath::Aos::Vector3 m_target; // position to look at
    Vectormath::Aos::Matrix4 m_viewMat; // final calculated view matrix
    float m_pitchSpeed; // rotation/sec speed of pitch
    float m_yaw; // yaw rotation in radians
    float m_yawSpeed; // rotation/sec speed of yaw
};

#endif // ORBIT_CAMERA_H_INCLUDED
