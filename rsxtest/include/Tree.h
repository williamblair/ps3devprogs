#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

#include <Entity.h>

class Tree : public Entity
{
public:

    Tree() :
        m_x( 0.0f ),
        m_y( 0.0f ),
        m_z( 0.0f )
    {}

    Tree( float x, float y, float z ) :
        m_x( x ),
        m_y( y ),
        m_z( z )
    {}

    virtual ~Tree();

    Tree& operator=( const Tree& t )
    {
        m_x = t.m_x;
        m_y = t.m_y;
        m_z = t.m_z;

        return *this;
    }

    bool Init( gcmContextData* context );

private:

    // TODO - use with entity position
    // TODO - add position/rotation/scale to entity
    float m_x, m_y, m_z;

    static void InitBuffers();
    static S3DVertex* s_vertices;
};

#endif // TREE_H_INCLUDED

