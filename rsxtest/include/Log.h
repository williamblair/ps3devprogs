#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <list>
#include <string>

#include "rsxutil.h"

#define LOG_STR_MAX 256
#define LOG_HISTORY_LEN 10

#define DBG_LOG(format, ...)                                            \
    sprintf( Log::dbgPrintStr, "%s:%d: ", __FILE__, __LINE__ );         \
    Log::len = strlen( Log::dbgPrintStr );                              \
    sprintf( &Log::dbgPrintStr[ Log::len ], format, ##__VA_ARGS__ );    \
                                                                        \
    printf( Log::dbgPrintStr );                                         \
                                                                        \
    Log::dbgLogHistory.push_back( Log::dbgPrintStr );                   \
    if ( Log::dbgLogHistory.size() > LOG_HISTORY_LEN ) {                \
        Log::dbgLogHistory.pop_front();                                 \
    }

namespace Log
{

extern char dbgPrintStr[LOG_STR_MAX];
extern std::list<std::string> dbgLogHistory;
extern int len;

inline void DebugRender( int startX, int startY, float r, float g, float b )
{
    DebugFont::setColor( r, g, b, 1.0f );
    for ( std::string& str : dbgLogHistory )
    {
        DebugFont::setPosition( startX, startY );
        DebugFont::print( str.c_str() );
        startY += 20;
    }
} 

} // end namespace Log

#endif // LOG_H_INCLUDED

