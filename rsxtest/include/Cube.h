#ifndef CUBE_H_INCLUDED
#define CUBE_H_INCLUDED

#include "Entity.h"

class Cube : public Entity
{
public:
    Cube();
    virtual ~Cube();

    bool Init( gcmContextData* context );
    
private:

    bool createCube( f32 size );
};

#endif // CUBE_H_INCLUDED

