#ifndef VECTOR2_H_INCLUDED
#define VECTOR2_H_INCLUDED

#include <math.h>

struct Vector2
{
    union
    {
        struct {
            f32 u,v;
        };
        struct {
            f32 x,y;
        };
    };
    

    Vector2() :
        u( 0.0f ),
        v( 0.0f )
    {}
    
    Vector2( f32 _u, f32 _v ) :
        u( _u ),
        v( _v )
    {}
    
    inline float Length()
    {
        return sqrtf( u*u + v*v );
    }
};

#endif // VECTOR2_H_INCLUDED

