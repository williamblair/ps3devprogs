#ifndef TERRAIN_H_INCLUDED
#define TERRAIN_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <assert.h>
#include <sys/process.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

#include <vector>

#include <Entity.h>
#include <Vector2.h>
#include <TargaImage.h>

#define DBGASSERT(x) assert(x)
//#define DBGASSERT(x)

class Terrain : public Entity
{
public:

    Terrain();
    virtual ~Terrain();

    bool Init( gcmContextData* context,
               const std::string& rawFileName,
               const unsigned int width = 65,
               const float heightScale = 10.0f,
               const float sizeScale = 1.0f );

    inline Vectormath::Aos::Vector3 GetPositionAt( int x, int z )
    {
        DBGASSERT( ( z * int( m_width ) ) + x < (int)m_positions.size() );
        return m_positions[ ( z * m_width ) + x ];
    }

private:

    std::vector<Vectormath::Aos::Vector3> m_positions;
    std::vector<float> m_heights;
    std::vector<Vectormath::Aos::Vector3> m_colors;
    std::vector<Vectormath::Aos::Vector3> m_normals;
    std::vector<Vector2> m_texCoords;
    std::vector<u32> m_indices;
    unsigned int m_width;
    float m_minX;
    float m_minZ;
    float m_maxX;
    float m_maxZ;

    std::vector<Vectormath::Aos::Vector3> m_waterPositions; 
    std::vector<u32> m_waterIndices;
    std::vector<Vector2> m_waterTexCoords;

    //Targa::Image m_texImage;

    void GenerateVertices( const float sizeScale );
    void GenerateTexCoords();
    void GenerateNormals();
    void GenerateIndices();
    bool GenerateBuffers();

    void GenerateWaterVertices();
    void GenerateWaterTexCoords();
    void GenerateWaterIndices();
};

#endif // TERRAIN_H_INCLUDED

