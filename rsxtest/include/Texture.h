#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

#include "acid.h"
#include "mesh.h"
#include "rsxutil.h"

class Texture
{
public:
    Texture();
    ~Texture();

    bool Init( gcmContextData* context, 
               u8* pixelData, 
               u32 width, 
               u32 height, 
               u32 bytesPerPixel );

    void Set( u8 shaderTexUnit );

protected:

    gcmContextData* m_context;
    
    u32* m_buffer;
    u32  m_offset;

    u32 m_width;
    u32 m_height;

    gcmTexture m_texture;
};

#endif // TEXTURE_H_INCLUDED

